use std::io::prelude::*;
use std::io::{stdin, BufReader};

use maths_lib::parser::{build_ast, evaluate, tokenize};

fn main() {
    let reader = BufReader::new(stdin());
    for line in reader.lines() {
        let tokens = tokenize(&line.unwrap());
        if let Ok(tokens) = tokens {
            let ast = build_ast(tokens);
            if let Ok(ast) = ast {
                let eval = evaluate(ast);
                if let Ok(eval) = eval {
                    println!("{}", eval);
                } else {
                    println!("{:?}", eval);
                }
            } else {
                println!("{:?}", ast);
            }
        } else {
            println!("{:?}", tokens);
        }
    }
}
