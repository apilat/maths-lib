use crate::algs::gcd;
use crate::bigfloat::Float as BigFloat;
use crate::bigint::{Sint, Uint};
use std::convert::{TryFrom, TryInto};
use std::fmt::{self, Display, Formatter};
use std::ops::{Add, Div, Mul, Neg, Sub};
use std::str::FromStr;

/// The most generic representation of a number in this library. Any operations will keep the
/// number exact as long as is supported (although radicals are not supported yet).
///
/// This interface is to be considered high-level, so unnecessary allocations could be made in any
/// operations. These are most likely, but not exclusively present when dealing with numbers of
/// different types.
///
/// The precision of floating point numbers is not configurable and set to 384 bits.
#[non_exhaustive]
#[derive(Debug, Clone)]
pub enum Number {
    Integer(Sint),
    /// Fraction `a/b` in its simplest form: `gcd(a, b) == 1`, `b > 0`.
    Fraction(Sint, Uint),
    Float(BigFloat<384>),
}

impl Number {
    pub fn into_integer(self) -> Option<Sint> {
        match self {
            Number::Integer(x) => Some(x),
            _ => None,
        }
    }

    pub fn into_fraction(self) -> Option<(Sint, Uint)> {
        match self {
            Number::Fraction(p, q) => Some((p, q)),
            _ => None,
        }
    }

    pub fn into_float(self) -> Option<BigFloat<384>> {
        match self {
            Number::Float(x) => Some(x),
            _ => None,
        }
    }

    pub fn as_integer(&self) -> Option<&Sint> {
        match self {
            Number::Integer(x) => Some(x),
            _ => None,
        }
    }

    pub fn as_fraction(&self) -> Option<(&Sint, &Uint)> {
        match self {
            Number::Fraction(p, q) => Some((p, q)),
            _ => None,
        }
    }

    pub fn as_float(&self) -> Option<&BigFloat<384>> {
        match self {
            Number::Float(x) => Some(x),
            _ => None,
        }
    }

    fn as_u64(&self) -> Option<u64> {
        match self {
            Number::Integer(x) => x.try_into().ok().and_then(|x: i64| u64::try_from(x).ok()),
            _ => None,
        }
    }

    /// Returns `true` if this number is equal to zero (exactly for integers and fraction and
    /// within the available precision for float).
    pub fn is_zero(&self) -> bool {
        match self {
            Number::Integer(x) => x.eq(&0),
            Number::Fraction(p, _) => p.eq(&0),
            // TODO Replace with f.eq(&0.0) for consistency when PartialOrd<f64> implemented for f
            Number::Float(f) => f.is_zero(),
        }
    }

    /// Attempts to take `self` to the power of `exponent`.
    /// Currently this fails if the exponent is not a positive integer, which is a limitation that will be
    /// removed in the future.
    // TODO Add approximations for fractional and floating point exponents
    pub fn try_pow(self, exponent: Number) -> Option<Self> {
        let exp = exponent.as_u64()?;
        match self {
            Number::Integer(x) => Some(Number::Integer(x.pow(exp))),
            Number::Fraction(p, q) => Some(reduce_fraction(p.pow(exp), q.pow(exp).into())),
            Number::Float(f) => Some(Number::Float(f.pow_u64(exp))),
        }
    }
}

impl Display for Number {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        use Number::*;
        match self {
            Integer(x) => x.fmt(f),
            Fraction(p, q) => write!(f, "{} / {}", p, q),
            // TODO Float Display
            Float(_) => write!(f, "Float (cannot be printed yet)"),
        }
    }
}

impl FromStr for Number {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, ()> {
        if let Ok(i) = Sint::from_decimal(s) {
            Ok(Number::Integer(i))
        // TODO Float::from_decimal(s)
        } else {
            Err(())
        }
    }
}

impl PartialEq for Number {
    /// Note: comparisons of exact values with Float will always return false.
    // FIXME Figure out how to efficiently compare values of different types.
    fn eq(&self, other: &Self) -> bool {
        use Number::*;
        match (self, other) {
            (Integer(a), Integer(b)) => a == b,
            (Integer(a), Fraction(p, q)) | (Fraction(p, q), Integer(a)) => q == &1 && a == p,
            (Fraction(p, q), Fraction(r, s)) => p == r && q == s,
            (Float(f), Float(f2)) => f == f2,
            (Float(_), _) | (_, Float(_)) => false,
        }
    }
}
impl Eq for Number {}

fn reduce_fraction(num: Sint, denom: Sint) -> Number {
    if num != 0 && denom == 0 {
        panic!("Fraction denominator cannot be equal to zero");
    }

    let g = gcd(num.abs.clone(), denom.abs.clone());

    let num = Sint {
        abs: num.abs / &g,
        neg: num.neg ^ denom.neg,
    };
    let denom = denom.abs / g;

    if denom == 1 {
        Number::Integer(num)
    } else {
        Number::Fraction(num, denom)
    }
}

impl Add<Number> for Number {
    type Output = Number;

    fn add(self, other: Number) -> Number {
        use Number::*;
        match (self, other) {
            (Integer(a), Integer(b)) => Integer(a + b),
            (Integer(a), Fraction(p, q)) | (Fraction(p, q), Integer(a)) => Fraction(p + a * &q, q),
            (Integer(a), Float(f)) | (Float(f), Integer(a)) => Float(f + BigFloat::from(a)),
            (Fraction(a, b), Fraction(c, d)) => reduce_fraction(a * &d + c * &b, Sint::from(b * d)),
            (Fraction(a, b), Float(f)) | (Float(f), Fraction(a, b)) => {
                Float(f + BigFloat::from(a) / BigFloat::from(b))
            }
            (Float(f), Float(f2)) => Float(f + f2),
        }
    }
}

impl Sub<Number> for Number {
    type Output = Number;

    fn sub(self, other: Number) -> Number {
        use Number::*;
        match other {
            Integer(a) => self + Integer(-a),
            Fraction(p, q) => self + Fraction(-p, q),
            Float(f) => self + Float(-f),
        }
    }
}

impl Mul<Number> for Number {
    type Output = Number;

    fn mul(self, other: Number) -> Number {
        use Number::*;
        match (self, other) {
            (Integer(a), Integer(b)) => Integer(a * b),
            (Integer(a), Fraction(p, q)) | (Fraction(p, q), Integer(a)) => {
                reduce_fraction(a * p, Sint::from(q))
            }
            (Integer(a), Float(f)) | (Float(f), Integer(a)) => Float(f * BigFloat::from(a)),
            (Fraction(a, b), Fraction(c, d)) => reduce_fraction(a * c, Sint::from(b * d)),
            (Fraction(a, b), Float(f)) | (Float(f), Fraction(a, b)) => {
                Float(f * BigFloat::from(a) / BigFloat::from(b))
            }
            (Float(f), Float(f2)) => Float(f * f2),
        }
    }
}

impl Div<Number> for Number {
    type Output = Number;

    fn div(self, other: Number) -> Number {
        use Number::*;
        match (self, other) {
            (Integer(a), Integer(b)) => reduce_fraction(a, b),
            (Integer(a), Fraction(p, q)) => reduce_fraction(a * q, p),
            (Fraction(p, q), Integer(a)) => reduce_fraction(p, a * q),
            (Integer(a), Float(f)) => Float(BigFloat::from(a) / f),
            (Float(f), Integer(a)) => Float(f / BigFloat::from(a)),
            (Fraction(a, b), Fraction(c, d)) => reduce_fraction(a * d, c * b),
            (Fraction(a, b), Float(f)) => Float(BigFloat::from(a) * f / BigFloat::from(b)),
            (Float(f), Fraction(a, b)) => Float(f * BigFloat::from(b) / BigFloat::from(a)),
            (Float(f), Float(f2)) => Float(f / f2),
        }
    }
}

impl Neg for Number {
    type Output = Number;

    fn neg(self) -> Number {
        use Number::*;
        match self {
            Integer(a) => Integer(-a),
            Fraction(p, q) => Fraction(-p, q),
            Float(f) => Float(-f),
        }
    }
}

#[cfg(test)]
mod tests {
    // FIXME tests
}
