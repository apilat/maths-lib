use crate::bigint::{Sint, Uint};
use std::convert::TryInto;

/// Returns the greatest common divisor of `a` and `b`.
///
/// # Edge cases
///
/// When one of the numbers is equal to zero, the other one is returned, as given by an
/// implementation of Euclid's algorithm.
pub fn gcd(mut a: Uint, mut b: Uint) -> Uint {
    while b != 0 {
        let t = b;
        b = a % &t;
        a = t;
    }
    a
}

/// Returns the lowest common multiple of `a` and `b`.
///
/// # Edge cases
///
/// When one or both of the numbers equal zero, the function returns zero.
pub fn lcm(a: Uint, b: Uint) -> Uint {
    if a == 0 && b == 0 {
        Uint::from(0)
    } else {
        let g = gcd(a.clone(), b.clone());
        a / g * b
    }
}

/// Performs the extended Euclidean algorithm to find the Bézout coefficients of `a` and `b`.
///
/// # Returns
///
/// A tuple of `(s, t, g)` which uniquely satisfies
/// `as + bt = g = gcd(a, b)`.
#[allow(clippy::many_single_char_names)]
pub fn extended_gcd(a: Uint, b: Uint) -> (Sint, Sint, Uint) {
    // Based on [https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm]
    let (mut old_r, mut r) = (a, b);
    let (mut old_s, mut s): (Sint, Sint) = (1.into(), 0.into());
    let (mut old_t, mut t): (Sint, Sint) = (0.into(), 1.into());

    while r != 0 {
        let (quotient, remainder) = old_r.div_rem(&r);
        old_r = r;
        r = remainder;

        let quotient = Sint::from(quotient);

        let tmp = s.clone();
        s = old_s - s * &quotient;
        old_s = tmp;

        let tmp = t.clone();
        t = old_t - t * &quotient;
        old_t = tmp;
    }

    (old_s, old_t, old_r)
}

/// Finds the modular multiplicative inverse of `a` modulo `n` if it exists.
///
/// # Panics
///
/// Panics if `a >= n`. You need to reduce `a` modulo `n` before calling this function.
pub fn modular_multiplicative_inverse(a: Uint, n: Uint) -> Option<Uint> {
    assert!(a < n);
    if a == 0 {
        return None;
    }

    let (s, _, g) = extended_gcd(a, n.clone());
    if g != 1 {
        None
    } else {
        Some(
            if s.le(&0) {
                s + Sint::from(n)
            } else {
                s
            }
            .try_into()
            .unwrap(),
        )
    }
}

#[cfg(test)]
mod tests {
    use crate::algs;
    use crate::bigint::{Sint, Uint};
    use std::panic::catch_unwind;

    #[test]
    fn gcd() {
        assert_eq!(
            algs::gcd(Uint::from_parts(vec![761, 54, 89]), Uint::from(0)),
            Uint::from_parts(vec![761, 54, 89])
        );
        assert_eq!(
            algs::gcd(Uint::from(0), Uint::from_parts(vec![0, 1, 2])),
            Uint::from_parts(vec![0, 1, 2])
        );

        assert_eq!(algs::gcd(Uint::from(1), Uint::from(2)), Uint::from(1));
        assert_eq!(algs::gcd(Uint::from(2), Uint::from(11)), Uint::from(1));
        assert_eq!(
            algs::gcd(Uint::from(65537), Uint::from(78467)),
            Uint::from(1)
        );
        assert_eq!(
            algs::gcd(Uint::from(65537), Uint::from(262148)),
            Uint::from(65537)
        );

        assert_eq!(
            algs::gcd(
                Uint::from_decimal("16035378745039072864230220234597980").unwrap(),
                Uint::from_decimal("71874694269225237576640810644273440").unwrap()
            ),
            Uint::from(115558157260)
        );
        assert_eq!(
            algs::gcd(
                Uint::from_decimal("653217342662348292657166598592439014").unwrap(),
                Uint::from_decimal("577271273365499873071361111149393110").unwrap()
            ),
            Uint::from(660625502178)
        );
    }

    #[test]
    fn lcm() {
        assert_eq!(algs::lcm(Uint::from(0), Uint::from(0)), Uint::from(0));
        assert_eq!(algs::lcm(Uint::from(17), Uint::from(0)), Uint::from(0));
        assert_eq!(algs::lcm(Uint::from(0), Uint::from(71)), Uint::from(0));

        assert_eq!(algs::lcm(Uint::from(1), Uint::from(819)), Uint::from(819));
        assert_eq!(algs::lcm(Uint::from(8), Uint::from(12)), Uint::from(24));

        assert_eq!(
            algs::lcm(Uint::from(1080018721013175), Uint::from(1073007395894990)),
            Uint::from_decimal("13633742062966877912337723450").unwrap()
        );
    }

    #[test]
    fn extended_gcd() {
        assert_eq!(
            algs::extended_gcd(Uint::from(0), Uint::from(3)),
            (Sint::from(0), Sint::from(1), Uint::from(3))
        );
        assert_eq!(
            algs::extended_gcd(Uint::from(15), Uint::from(0)),
            (Sint::from(1), Sint::from(0), Uint::from(15))
        );

        assert_eq!(
            algs::extended_gcd(Uint::from(7), Uint::from(9)),
            (Sint::from(4), Sint::from(-3), Uint::from(1))
        );
        assert_eq!(
            algs::extended_gcd(Uint::from(12), Uint::from(3)),
            (Sint::from(0), Sint::from(1), Uint::from(3))
        );

        assert_eq!(
            algs::extended_gcd(Uint::from(1287623), Uint::from(549102893)),
            (Sint::from(231600350), Sint::from(-543093), Uint::from(1))
        );
        assert_eq!(
            algs::extended_gcd(Uint::from(176328637604387), Uint::from(124278612331469)),
            (
                Sint::from(-29171064986261),
                Sint::from(41388409880032),
                Uint::from(1)
            )
        );
    }

    #[test]
    fn modinv() {
        use algs::modular_multiplicative_inverse as mmi;
        assert_eq!(mmi(Uint::from(1), Uint::from(4)), Some(Uint::from(1)));
        assert_eq!(mmi(Uint::from(2), Uint::from(5)), Some(Uint::from(3)));
        assert_eq!(mmi(Uint::from(5), Uint::from(7)), Some(Uint::from(3)));
        assert_eq!(mmi(Uint::from(0), Uint::from(17)), None);
        assert_eq!(mmi(Uint::from(6), Uint::from(10)), None);

        assert_eq!(
            mmi(Uint::from(115652), Uint::from(788305086821)),
            Some(Uint::from(701241993455))
        );
        assert_eq!(
            mmi(
                Uint::from_parts(vec![66, 102]),
                Uint::from_parts(vec![9192, 771])
            ),
            None
        );
        assert_eq!(
            mmi(
                Uint::from_decimal("819378714883818020654846668").unwrap(),
                Uint::from_decimal("1440974646102341098906564425").unwrap()
            ),
            Some(Uint::from_decimal("1221100596438793559341886482").unwrap())
        );

        assert!(catch_unwind(|| mmi(Uint::from(0), Uint::from(0))).is_err());
        assert!(catch_unwind(|| mmi(Uint::from(123), Uint::from(0))).is_err());
    }
}
