use crate::bigint::Uint;
use crate::crypto::{is_prime, RandomOptionsBuilder, Rsa, RsaError};
use crate::number::Number;
use std::convert::TryInto;
use std::fmt::{self, Display, Formatter};
use std::iter::Peekable;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Token {
    Number(Number),
    Identifier(String),
    Paren(bool),
    Operator(Operator),
    Comma,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Operator {
    Plus,
    Minus,
    Star,
    StarStar,
    Slash,
    Percent,
    Caret,
    ShiftRight,
    ShiftLeft,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum TokenizerError {
    UnknownToken(String),
    Unsupported(&'static str),
}

/// Split the given string into tokens.
///
/// # Note
///
/// This function does not attempt to consolide unary minus signs into numbers, as that has proven
/// to be error-prone. Instead, a separate `Operator::Minus` will be emitted and all numbers will
/// be positive.
///
/// # Returns
///
/// `Error::UnknownToken` if any part of the input cannot be parsed as a correct token.
/// `Error::Unsupported` if a token is parsed that is not yet supported.
/// A `Vec<Token>` containing parsed tokens if the operation succeeds.
pub fn tokenize(s: &str) -> Result<Vec<Token>, TokenizerError> {
    let mut vec = Vec::<Token>::new();
    let mut iter = s.chars().peekable();

    while let Some(c) = iter.next() {
        vec.push(match c {
            '+' => Token::Operator(Operator::Plus),
            '-' => Token::Operator(Operator::Minus),
            '*' => match iter.peek() {
                Some('*') => {
                    iter.next().unwrap();
                    Token::Operator(Operator::StarStar)
                }
                _ => Token::Operator(Operator::Star),
            },
            '/' => Token::Operator(Operator::Slash),
            '%' => Token::Operator(Operator::Percent),
            '^' => Token::Operator(Operator::Caret),
            '(' | '[' | '{' => Token::Paren(true),
            ')' | ']' | '}' => Token::Paren(false),
            '<' if iter.peek() == Some(&'<') => {
                iter.next().unwrap();
                Token::Operator(Operator::ShiftLeft)
            }
            '>' if iter.peek() == Some(&'>') => {
                iter.next().unwrap();
                Token::Operator(Operator::ShiftRight)
            }
            ',' => Token::Comma,
            _ if c.is_alphabetic() => {
                let mut intermediate = String::new();
                intermediate.push(c);

                while let Some(c) = iter.peek() {
                    if c.is_alphanumeric() || c == &'_' {
                        intermediate.push(iter.next().unwrap());
                    } else {
                        break;
                    }
                }

                Token::Identifier(intermediate)
            }
            _ if c.is_numeric() => {
                let mut intermediate = String::new();
                intermediate.push(c);

                while let Some(c) = iter.peek() {
                    match c {
                        '.' | 'e' | '0'..='9' => {
                            intermediate.push(iter.next().unwrap());
                        }
                        _ => break,
                    }
                }

                if let Ok(n) = intermediate.parse() {
                    Token::Number(n)
                } else {
                    return Err(TokenizerError::UnknownToken(intermediate));
                }
            }
            _ if c.is_whitespace() => continue,
            _ => return Err(TokenizerError::UnknownToken(c.to_string())),
        })
    }

    Ok(vec)
}

#[cfg(test)]
mod tokenizer_tests {
    use super::{tokenize, Operator::*, Token::*, TokenizerError as Error};
    use crate::bigint::Uint;
    use crate::number::Number::Integer as I;

    #[test]
    fn ignore_whitespace() {
        assert_eq!(tokenize(" \t \n \r "), Ok(vec![]));
    }

    #[test]
    fn identifiers() {
        assert_eq!(
            tokenize("abc de"),
            Ok(vec![
                Identifier("abc".to_string()),
                Identifier("de".to_string())
            ])
        );
        assert_eq!(
            tokenize("x1\ty77a"),
            Ok(vec![
                Identifier("x1".to_string()),
                Identifier("y77a".to_string())
            ])
        );
    }

    #[test]
    fn operators() {
        assert_eq!(
            tokenize("+-\r/*%^"),
            Ok(vec![
                Operator(Plus),
                Operator(Minus),
                Operator(Slash),
                Operator(Star),
                Operator(Percent),
                Operator(Caret)
            ])
        );
        assert_eq!(
            tokenize("2 * 3 ** 5"),
            Ok(vec![
                Number(I(2.into())),
                Operator(Star),
                Number(I(3.into())),
                Operator(StarStar),
                Number(I(5.into()))
            ])
        );
        assert_eq!(
            tokenize("3 << 7 >> 4"),
            Ok(vec![
                Number(I(3.into())),
                Operator(ShiftLeft),
                Number(I(7.into())),
                Operator(ShiftRight),
                Number(I(4.into()))
            ])
        );
        assert_eq!(
            tokenize("3-4+5"),
            Ok(vec![
                Number(I(3.into())),
                Operator(Minus),
                Number(I(4.into())),
                Operator(Plus),
                Number(I(5.into()))
            ]),
        );
    }

    #[test]
    fn numbers() {
        assert_eq!(
            tokenize("776 1 09742 894032890819203784937209483290847"),
            Ok(vec![
                Number(I(Uint::from_parts(vec![776]).into())),
                Number(I(Uint::from_parts(vec![1]).into())),
                Number(I(Uint::from_parts(vec![9742]).into())),
                Number(I(Uint::from_parts(vec![
                    15251381326673668319,
                    48465620124983
                ])
                .into()))
            ])
        );
        assert_eq!(
            tokenize("776.0 1.23e4 0.00532"),
            Err(Error::UnknownToken("776.0".to_owned()))
        );
        assert_eq!(
            tokenize("+112 --743"),
            Ok(vec![
                Operator(Plus),
                Number(I(112.into())),
                Operator(Minus),
                Operator(Minus),
                Number(I(743.into()))
            ])
        );
    }

    #[test]
    fn parens() {
        assert_eq!(
            tokenize("())] {[ ] }"),
            Ok(vec![
                Paren(true),
                Paren(false),
                Paren(false),
                Paren(false),
                Paren(true),
                Paren(true),
                Paren(false),
                Paren(false)
            ])
        );
    }

    #[test]
    fn invalid() {
        assert_eq!(
            tokenize("7 ?:#"),
            Err(Error::UnknownToken(String::from("?")))
        );
        assert_eq!(
            tokenize("123e 4.3.2 1ee3"),
            Err(Error::UnknownToken(String::from("123e")))
        );
    }

    #[test]
    fn full() {
        assert_eq!(
            tokenize("(123 + x) * 3 / [2/7]"),
            Ok(vec![
                Paren(true),
                Number(I(Uint::from_parts(vec![123]).into())),
                Operator(Plus),
                Identifier("x".to_string()),
                Paren(false),
                Operator(Star),
                Number(I(Uint::from_parts(vec![3]).into())),
                Operator(Slash),
                Paren(true),
                Number(I(Uint::from_parts(vec![2]).into())),
                Operator(Slash),
                Number(I(Uint::from_parts(vec![7]).into())),
                Paren(false)
            ])
        );
        assert_eq!(
            tokenize("{2b} ^ (4a/fun)"),
            Ok(vec![
                Paren(true),
                Number(I(Uint::from_parts(vec![2]).into())),
                Identifier("b".to_string()),
                Paren(false),
                Operator(Caret),
                Paren(true),
                Number(I(Uint::from_parts(vec![4]).into())),
                Identifier("a".to_string()),
                Operator(Slash),
                Identifier("fun".to_string()),
                Paren(false)
            ])
        );
        assert_eq!(
            tokenize("E=mc^2"),
            Err(Error::UnknownToken(String::from("=")))
        );
        assert_eq!(
            tokenize("4xyz** 2 - (-1) ^ 3"),
            Ok(vec![
                Number(I(4.into())),
                Identifier("xyz".to_string()),
                Operator(StarStar),
                Number(I(2.into())),
                Operator(Minus),
                Paren(true),
                Operator(Minus),
                Number(I(1.into())),
                Paren(false),
                Operator(Caret),
                Number(I(3.into()))
            ])
        );
        assert_eq!(
            tokenize("2x << 4 + 7 * 3 ** 2a"),
            Ok(vec![
                Number(I(2.into())),
                Identifier("x".to_string()),
                Operator(ShiftLeft),
                Number(I(4.into())),
                Operator(Plus),
                Number(I(7.into())),
                Operator(Star),
                Number(I(3.into())),
                Operator(StarStar),
                Number(I(2.into())),
                Identifier("a".to_string())
            ])
        );
        assert_eq!(
            tokenize("3-pow(4,10)"),
            Ok(vec![
                Number(I(3.into())),
                Operator(Minus),
                Identifier("pow".to_string()),
                Paren(true),
                Number(I(4.into())),
                Comma,
                Number(I(10.into())),
                Paren(false)
            ])
        );
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Expression {
    BinaryOp(BinaryOp, Box<Expression>, Box<Expression>),
    UnaryOp(UnaryOp, Box<Expression>),
    Number(Number),
    Identifier(String),
    Function(Function, Vec<Expression>),
    Result(EvaluateResult),
}

impl From<EvaluateResult> for Expression {
    fn from(val: EvaluateResult) -> Expression {
        Expression::Result(val)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum BinaryOp {
    Add,
    Subtract,
    Multiply,
    Divide,
    Pow,
    Modulo,
    ShiftLeft,
    ShiftRight,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum UnaryOp {
    Negate,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Function {
    Pow,
    Modulo,
    Random,
    IsPrime,
    RsaContext,
    RsaEncrypt,
    RsaDecrypt,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ParserError {
    UnexpectedEOS,
    UnexpectedToken(Token),
    NonmatchingParen,
    ExcessToken(Token),
    AmbiguousAssociativity,
}

type R = Result<Expression, ParserError>;
type I<'a> = &'a mut Peekable<std::vec::IntoIter<Token>>;

/// Build an abstract syntax tree of `Expression`s from the given tokens.
///
/// # Returns
///
/// `Ok(Expression)` representing the built AST if the operation succeeds.
/// `Err(Error)` containing more details of the failure otherwise.
pub fn build_ast(tokens: Vec<Token>) -> R {
    let mut iter = tokens.into_iter().peekable();
    let expr = parse_expression(&mut iter)?;
    match iter.next() {
        Some(x) => Err(ParserError::ExcessToken(x)),
        None => Ok(expr),
    }
}

fn parse_expression(iter: I) -> R {
    parse_addition(iter)
}

fn parse_addition(iter: I) -> R {
    let mut expr = parse_multiplication(iter)?;
    while let Some(Token::Operator(Operator::Plus)) | Some(Token::Operator(Operator::Minus)) =
        iter.peek()
    {
        if let Token::Operator(op) = iter.next().unwrap() {
            expr = Expression::BinaryOp(
                match op {
                    Operator::Plus => BinaryOp::Add,
                    Operator::Minus => BinaryOp::Subtract,
                    _ => unreachable!(),
                },
                Box::new(expr),
                Box::new(parse_multiplication(iter)?),
            );
        } else {
            unreachable!();
        }
    }
    Ok(expr)
}

fn parse_multiplication(iter: I) -> R {
    let mut expr = parse_shift(iter)?;
    while let Some(Token::Operator(Operator::Star))
    | Some(Token::Operator(Operator::Slash))
    | Some(Token::Operator(Operator::Percent)) = iter.peek()
    {
        if let Token::Operator(op) = iter.next().unwrap() {
            expr = Expression::BinaryOp(
                match op {
                    Operator::Star => BinaryOp::Multiply,
                    Operator::Slash => BinaryOp::Divide,
                    Operator::Percent => BinaryOp::Modulo,
                    _ => unreachable!(),
                },
                Box::new(expr),
                Box::new(parse_shift(iter)?),
            );
        } else {
            unreachable!();
        }
    }
    Ok(expr)
}

fn parse_shift(iter: I) -> R {
    let mut expr = parse_exponent(iter)?;
    if let Some(Token::Operator(Operator::ShiftLeft))
    | Some(Token::Operator(Operator::ShiftRight)) = iter.peek()
    {
        if let Token::Operator(op) = iter.next().unwrap() {
            expr = Expression::BinaryOp(
                match op {
                    Operator::ShiftLeft => BinaryOp::ShiftLeft,
                    Operator::ShiftRight => BinaryOp::ShiftRight,
                    _ => unreachable!(),
                },
                Box::new(expr),
                Box::new(parse_exponent(iter)?),
            );
        } else {
            unreachable!();
        }
    }
    if let Some(Token::Operator(Operator::ShiftLeft))
    | Some(Token::Operator(Operator::ShiftRight)) = iter.peek()
    {
        Err(ParserError::AmbiguousAssociativity)
    } else {
        Ok(expr)
    }
}

fn parse_exponent(iter: I) -> R {
    let mut expr = parse_base(iter)?;
    if let Some(Token::Operator(Operator::Caret)) | Some(Token::Operator(Operator::StarStar)) =
        iter.peek()
    {
        iter.next().unwrap();
        expr = Expression::BinaryOp(BinaryOp::Pow, Box::new(expr), Box::new(parse_base(iter)?));
    }
    if let Some(Token::Operator(Operator::Caret)) | Some(Token::Operator(Operator::StarStar)) =
        iter.peek()
    {
        Err(ParserError::AmbiguousAssociativity)
    } else {
        Ok(expr)
    }
}

fn parse_base(iter: I) -> R {
    match iter.peek() {
        None => Err(ParserError::UnexpectedEOS),
        Some(x) => match x {
            Token::Number(_) => Ok(Expression::Number(
                if let Token::Number(x) = iter.next().unwrap() {
                    x
                } else {
                    unreachable!()
                },
            )),
            Token::Identifier(_) => {
                let name = if let Token::Identifier(x) = iter.next().unwrap() {
                    x
                } else {
                    unreachable!()
                };
                match match name.as_str() {
                    "pow" => Some(Function::Pow),
                    "mod" => Some(Function::Modulo),
                    "rand" => Some(Function::Random),
                    "isprime" => Some(Function::IsPrime),
                    "rsa_gen" => Some(Function::RsaContext),
                    "rsa_encrypt" => Some(Function::RsaEncrypt),
                    "rsa_decrypt" => Some(Function::RsaDecrypt),
                    _ => None,
                } {
                    Some(function) => {
                        Ok(Expression::Function(function, parse_function_args(iter)?))
                    }
                    None => Ok(Expression::Identifier(name)),
                }
            }
            Token::Paren(true) => {
                iter.next().unwrap();
                let expr = parse_expression(iter)?;
                match iter.peek() {
                    Some(Token::Paren(false)) => {
                        iter.next().unwrap();
                        Ok(expr)
                    }
                    Some(t) => Err(ParserError::UnexpectedToken(t.clone())),
                    None => Err(ParserError::UnexpectedEOS),
                }
            }
            Token::Paren(false) => Err(ParserError::NonmatchingParen),
            Token::Operator(Operator::Minus) => {
                iter.next().unwrap();
                Ok(Expression::UnaryOp(
                    UnaryOp::Negate,
                    Box::new(parse_base(iter)?),
                ))
            }
            t @ Token::Operator(_) | t @ Token::Comma => {
                Err(ParserError::UnexpectedToken(t.clone()))
            }
        },
    }
}

fn parse_function_args(iter: I) -> Result<Vec<Expression>, ParserError> {
    let mut args = Vec::new();
    let has_parens = if let Some(Token::Paren(true)) = iter.peek() {
        iter.next().unwrap();
        true
    } else {
        false
    };

    while let Ok(expr) = parse_expression(iter) {
        args.push(expr);
        if let Some(Token::Comma) = iter.peek() {
            iter.next().unwrap();
        } else {
            break;
        }
    }

    if has_parens {
        match iter.next() {
            Some(Token::Paren(false)) => (),
            Some(t) => return Err(ParserError::UnexpectedToken(t)),
            None => return Err(ParserError::UnexpectedEOS),
        }
    }

    Ok(args)
}

#[cfg(test)]
mod parser_tests {
    use super::{
        build_ast as build, tokenize, BinaryOp::*, Expression::*, Function as F, Operator,
        ParserError as Error, Token as T, UnaryOp::*,
    };
    use crate::bigint::{Sint, Uint};
    use crate::number::Number::Integer as I;

    #[test]
    fn build_ast() {
        assert_eq!(
            build(vec![
                T::Identifier("x".to_string()),
                T::Operator(Operator::Plus),
                T::Number(I(Sint::from(0)))
            ]),
            Ok(BinaryOp(
                Add,
                Box::new(Identifier("x".to_string())),
                Box::new(Number(I(0.into())))
            ))
        );

        assert_eq!(
            build(tokenize("1 + 2 + 3").unwrap()),
            Ok(BinaryOp(
                Add,
                Box::new(BinaryOp(
                    Add,
                    Box::new(Number(I(1.into()))),
                    Box::new(Number(I(2.into())))
                )),
                Box::new(Number(I(3.into())))
            ))
        );
        assert_eq!(
            build(tokenize("2 * 3 + 4").unwrap()),
            Ok(BinaryOp(
                Add,
                Box::new(BinaryOp(
                    Multiply,
                    Box::new(Number(I(2.into()))),
                    Box::new(Number(I(3.into())))
                )),
                Box::new(Number(I(4.into())))
            ))
        );
        assert_eq!(
            build(tokenize("2 * (3 + 4)").unwrap()),
            Ok(BinaryOp(
                Multiply,
                Box::new(Number(I(2.into()))),
                Box::new(BinaryOp(
                    Add,
                    Box::new(Number(I(3.into()))),
                    Box::new(Number(I(4.into())))
                ))
            ))
        );
        assert_eq!(
            build(tokenize("2 ^ 3 + a % 4").unwrap()),
            Ok(BinaryOp(
                Add,
                Box::new(BinaryOp(
                    Pow,
                    Box::new(Number(I(2.into()))),
                    Box::new(Number(I(3.into())))
                )),
                Box::new(BinaryOp(
                    Modulo,
                    Box::new(Identifier("a".to_string())),
                    Box::new(Number(I(4.into())))
                ))
            ))
        );

        assert_eq!(
            build(tokenize("x / 2 * (3 + 4 - a)").unwrap()),
            Ok(BinaryOp(
                Multiply,
                Box::new(BinaryOp(
                    Divide,
                    Box::new(Identifier("x".to_string())),
                    Box::new(Number(I(2.into())))
                )),
                Box::new(BinaryOp(
                    Subtract,
                    Box::new(BinaryOp(
                        Add,
                        Box::new(Number(I(3.into()))),
                        Box::new(Number(I(4.into())))
                    )),
                    Box::new(Identifier("a".to_string()))
                ))
            ))
        );
        assert_eq!(
            build(tokenize("2 * 7 % 4").unwrap()),
            Ok(BinaryOp(
                Modulo,
                Box::new(BinaryOp(
                    Multiply,
                    Box::new(Number(I(2.into()))),
                    Box::new(Number(I(7.into())))
                )),
                Box::new(Number(I(4.into())))
            ))
        );
        assert_eq!(
            build(tokenize("2 ^ 3 ^ 4").unwrap()),
            Err(Error::AmbiguousAssociativity)
        );

        assert_eq!(
            build(tokenize("3 * x << 7 ** 5 + 3").unwrap()),
            Ok(BinaryOp(
                Add,
                Box::new(BinaryOp(
                    Multiply,
                    Box::new(Number(I(3.into()))),
                    Box::new(BinaryOp(
                        ShiftLeft,
                        Box::new(Identifier("x".to_string())),
                        Box::new(BinaryOp(
                            Pow,
                            Box::new(Number(I(7.into()))),
                            Box::new(Number(I(5.into())))
                        )),
                    ))
                )),
                Box::new(Number(I(3.into())))
            ))
        );
        assert_eq!(
            build(tokenize("2 << 32 >> 4").unwrap()),
            Err(Error::AmbiguousAssociativity)
        );

        assert_eq!(
            build(tokenize("2*-3 + -(4 - x)").unwrap()),
            Ok(BinaryOp(
                Add,
                Box::new(BinaryOp(
                    Multiply,
                    Box::new(Number(I(2.into()))),
                    Box::new(UnaryOp(Negate, Box::new(Number(I(3.into())))))
                )),
                Box::new(UnaryOp(
                    Negate,
                    Box::new(BinaryOp(
                        Subtract,
                        Box::new(Number(I(4.into()))),
                        Box::new(Identifier("x".to_string()))
                    ))
                ))
            ))
        );

        assert_eq!(
            build(tokenize("1 2").unwrap()),
            Err(Error::ExcessToken(T::Number(I(
                Uint::from_parts(vec![2]).into()
            ))))
        );
        assert_eq!(
            build(tokenize("3 + 4 + ").unwrap()),
            Err(Error::UnexpectedEOS)
        );
        assert_eq!(
            build(tokenize("5 * (0 + ").unwrap()),
            Err(Error::UnexpectedEOS)
        );
        assert_eq!(
            build(tokenize("8)").unwrap()),
            Err(Error::ExcessToken(T::Paren(false)))
        );
        assert_eq!(
            build(tokenize("8 + )").unwrap()),
            Err(Error::NonmatchingParen)
        );
        matches!(build(tokenize("3 + -").unwrap()), Err(_));
    }

    #[test]
    fn function() {
        assert_eq!(
            build(tokenize("pow 40,10").unwrap()),
            Ok(Function(
                F::Pow,
                vec![Number(I(40.into())), Number(I(10.into()))]
            ))
        );
        assert_eq!(
            build(tokenize("mod pow(10, 20), 30").unwrap()),
            Ok(Function(
                F::Modulo,
                vec![
                    Function(F::Pow, vec![Number(I(10.into())), Number(I(20.into()))]),
                    Number(I(30.into()))
                ]
            ))
        );
        assert_eq!(
            build(tokenize("mod 12 % 5, 3 + 4 * 5, 0").unwrap()),
            Ok(Function(
                F::Modulo,
                vec![
                    BinaryOp(
                        Modulo,
                        Box::new(Number(I(12.into()))),
                        Box::new(Number(I(5.into())))
                    ),
                    BinaryOp(
                        Add,
                        Box::new(Number(I(3.into()))),
                        Box::new(BinaryOp(
                            Multiply,
                            Box::new(Number(I(4.into()))),
                            Box::new(Number(I(5.into())))
                        ))
                    ),
                    Number(I(0.into()))
                ]
            ))
        );
        assert_eq!(
            build(tokenize("2 * pow(24, 0)").unwrap()),
            Ok(BinaryOp(
                Multiply,
                Box::new(Number(I(2.into()))),
                Box::new(Function(
                    F::Pow,
                    vec![Number(I(24.into())), Number(I(0.into()))]
                ))
            ))
        );
        assert_eq!(
            build(tokenize("rand * 2").unwrap()),
            Ok(BinaryOp(
                Multiply,
                Box::new(Function(F::Random, vec![])),
                Box::new(Number(I(2.into())))
            ))
        );
        assert_eq!(
            build(tokenize("rand(128 + 3)").unwrap()),
            Ok(Function(
                F::Random,
                vec![BinaryOp(
                    Add,
                    Box::new(Number(I(128.into()))),
                    Box::new(Number(I(3.into())))
                )]
            ))
        );
        assert_eq!(
            build(tokenize("isprime 1771").unwrap()),
            Ok(Function(F::IsPrime, vec![Number(I(1771.into()))]))
        );

        assert_eq!(
            build(tokenize("pow 10 2").unwrap()),
            Err(Error::ExcessToken(T::Number(I(2.into()))))
        );

        assert_eq!(
            build(tokenize("rsa_encrypt rsa_gen(), 100").unwrap()),
            Ok(Function(
                F::RsaEncrypt,
                vec![Function(F::RsaContext, vec![]), Number(I(100.into()))]
            ))
        );
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
/// The result of an evaluated expression.
///
/// All arithmetic operaions return `Uint` but some function might return other types of result.
/// This type implements `Display` which works sensibly for all possible outputs.
pub enum EvaluateResult {
    Number(Number),
    Bool(bool),
    RsaContext(Rsa),
}

impl EvaluateResult {
    pub fn into_number(self) -> Result<Number, EvaluateError> {
        match self {
            EvaluateResult::Number(x) => Ok(x),
            _ => Err(EvaluateError::IncorrectResultType),
        }
    }

    pub fn into_uint(self) -> Result<Uint, EvaluateError> {
        self.into_number()?
            .into_integer()
            .ok_or(EvaluateError::NotAnInteger)?
            .try_into()
            .map_err(|()| EvaluateError::NegativeInteger)
    }

    pub fn into_bool(self) -> Result<bool, EvaluateError> {
        match self {
            EvaluateResult::Bool(x) => Ok(x),
            _ => Err(EvaluateError::IncorrectResultType),
        }
    }

    pub fn into_rsa_context(self) -> Result<Rsa, EvaluateError> {
        match self {
            EvaluateResult::RsaContext(x) => Ok(x),
            _ => Err(EvaluateError::IncorrectResultType),
        }
    }

    pub fn into_u64(self) -> Result<u64, EvaluateError> {
        self.into_uint()?
            .try_into()
            .map_err(|()| EvaluateError::OutsideU64Range)
    }
}

impl From<Number> for EvaluateResult {
    fn from(value: Number) -> Self {
        Self::Number(value)
    }
}

impl From<bool> for EvaluateResult {
    fn from(value: bool) -> Self {
        Self::Bool(value)
    }
}

impl From<Rsa> for EvaluateResult {
    fn from(value: Rsa) -> Self {
        Self::RsaContext(value)
    }
}

impl Display for EvaluateResult {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            Self::Number(x) => write!(f, "{}", x),
            Self::Bool(x) => write!(f, "{}", if *x { "True" } else { "False" }),
            Self::RsaContext(x) => {
                writeln!(f, "Modulus: {}", x.get_modulus())?;
                writeln!(f, "Exponent: {}", x.get_exponent())?;
                match x.get_private_key() {
                    Some(x) => writeln!(f, "Private key: {}", x),
                    None => writeln!(f, "Private key: <empty>"),
                }
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum EvaluateError {
    UndefinedIdentifier(String),
    DivisionByZero,
    IncorrectFunctionArgumentCount,
    IncorrectResultType,
    RsaError(RsaError),

    NotAnInteger,
    NegativeInteger,
    OutsideU64Range,
}

/// Evaluate an `Expression` to a numerical (or otherwise) result.
///
/// # Returns
///
/// `EvaluateError` containing more details if an error occurred during evaluation.
/// `Ok(EvaluateResult)` if the evaluation succeeds.
pub fn evaluate(expr: Expression) -> Result<EvaluateResult, EvaluateError> {
    match expr {
        Expression::Result(x) => Ok(x),
        Expression::Number(x) => Ok(x.into()),
        Expression::Identifier(s) => Err(EvaluateError::UndefinedIdentifier(s)),
        Expression::BinaryOp(op, expr1, expr2) => {
            evaluate_binop(op, evaluate(*expr1)?, evaluate(*expr2)?).map(EvaluateResult::Number)
        }
        Expression::Function(f, args) => {
            let args = args.into_iter().try_fold(Vec::new(), |mut v, x| {
                v.push(evaluate(x)?);
                Ok(v)
            })?;
            let len = args.len();
            evaluate_function(f, args.into_iter(), len)
        }
        Expression::UnaryOp(op, expr) => {
            evaluate_unaryop(op, evaluate(*expr)?.into_number()?).map(EvaluateResult::Number)
        }
    }
}

fn evaluate_binop(
    op: BinaryOp,
    e1: EvaluateResult,
    e2: EvaluateResult,
) -> Result<Number, EvaluateError> {
    match op {
        BinaryOp::Add => Ok(e1.into_number()? + e2.into_number()?),
        BinaryOp::Subtract => Ok(e1.into_number()? - e2.into_number()?),
        BinaryOp::Multiply => Ok(e1.into_number()? * e2.into_number()?),
        BinaryOp::Divide => {
            let e2 = e2.into_number()?;
            if !e2.is_zero() {
                Ok(e1.into_number()? / e2)
            } else {
                Err(EvaluateError::DivisionByZero)
            }
        }
        BinaryOp::Pow => e1
            .into_number()?
            .try_pow(e2.into_number()?)
            .ok_or(EvaluateError::OutsideU64Range),
        BinaryOp::Modulo => {
            let e2 = e2.into_uint()?;
            if e2.eq(&0) {
                Err(EvaluateError::DivisionByZero)
            } else {
                Ok(Number::Integer((e1.into_uint()? % e2).into()))
            }
        }
        BinaryOp::ShiftLeft => Ok(Number::Integer(
            (e1.into_uint()?
                << e2
                    .into_u64()?
                    .try_into()
                    .expect("u64 does not fit into usize"))
            .into(),
        )),
        BinaryOp::ShiftRight => Ok(Number::Integer(
            (e1.into_uint()?
                >> e2
                    .into_u64()?
                    .try_into()
                    .expect("u64 does not fit into usize"))
            .into(),
        )),
    }
}

fn evaluate_unaryop(op: UnaryOp, n: Number) -> Result<Number, EvaluateError> {
    match op {
        UnaryOp::Negate => Ok(-n),
    }
}

fn evaluate_function(
    f: Function,
    mut args: impl Iterator<Item = EvaluateResult>,
    arg_count: usize,
) -> Result<EvaluateResult, EvaluateError> {
    let mut next = || args.next().unwrap();
    match f {
        Function::Pow => {
            if arg_count == 2 {
                Ok(next()
                    .into_number()?
                    .try_pow(next().into_number()?)
                    .ok_or(EvaluateError::OutsideU64Range)?
                    .into())
            } else if arg_count == 3 {
                Ok(Number::Integer(
                    next()
                        .into_uint()?
                        .pow_mod(&next().into_uint()?, &next().into_uint()?)
                        .into(),
                )
                .into())
            } else {
                Err(EvaluateError::IncorrectFunctionArgumentCount)
            }
        }
        Function::Modulo => {
            if arg_count == 2 {
                Ok(Number::Integer((next().into_uint()? % next().into_uint()?).into()).into())
            } else {
                Err(EvaluateError::IncorrectFunctionArgumentCount)
            }
        }
        Function::Random => {
            // TODO Think of a way to generate random non-integers
            if arg_count == 0 {
                Ok(Number::Integer(RandomOptionsBuilder::new().bits(64).gen().into()).into())
            } else if arg_count == 1 {
                Ok(Number::Integer(
                    RandomOptionsBuilder::new()
                        .bits(next().into_u64()? as usize)
                        .gen()
                        .into(),
                )
                .into())
            } else {
                Err(EvaluateError::IncorrectFunctionArgumentCount)
            }
        }
        Function::IsPrime => {
            if arg_count == 1 {
                Ok(is_prime(&next().into_uint()?).into())
            } else {
                Err(EvaluateError::IncorrectFunctionArgumentCount)
            }
        }
        Function::RsaContext => {
            if arg_count == 0 {
                Ok(Rsa::default().into())
            } else if arg_count == 2 {
                Ok(Rsa::public(next().into_uint()?, next().into_u64()?).into())
            } else if arg_count == 3 {
                Ok(
                    Rsa::private(next().into_uint()?, next().into_u64()?, next().into_uint()?)
                        .into(),
                )
            } else {
                Err(EvaluateError::IncorrectFunctionArgumentCount)
            }
        }
        f @ Function::RsaEncrypt | f @ Function::RsaDecrypt => {
            if arg_count == 2 {
                let f = match f {
                    Function::RsaEncrypt => Rsa::encrypt,
                    Function::RsaDecrypt => Rsa::decrypt,
                    _ => unreachable!(),
                };
                Ok(Number::Integer(
                    f(&next().into_rsa_context()?, next().into_uint()?)
                        .map_err(EvaluateError::RsaError)?
                        .into(),
                )
                .into())
            } else {
                Err(EvaluateError::IncorrectFunctionArgumentCount)
            }
        }
    }
}

/// A helper function to evaluate a string by calling `tokenize`, `build_ast`, and `evaluate` in
/// series.
///
/// TODO this function will return `Box<Error>` instead of panicking when `std::error::Error` is
/// implemented on all intermediate error types.
pub fn evaluate_str(s: &str) -> EvaluateResult {
    evaluate(build_ast(tokenize(s).unwrap()).unwrap()).unwrap()
}

#[cfg(test)]
mod evaluate_tests {
    use super::{
        evaluate as eval, evaluate_str as eval_str,
        BinaryOp::*,
        EvaluateError as Error,
        EvaluateResult::{self as R, Bool as B, Number as N},
        Expression::{self, *},
        Function as F,
    };
    use crate::bigint::{Sint, Uint};
    use crate::crypto::RsaError;
    use crate::number::Number::{Fraction as Fr, Integer as I};
    use std::panic::catch_unwind;

    #[test]
    fn add() {
        assert_eq!(
            eval(Number(I(123.into()))),
            Ok(N(I(Uint::from_parts(vec![123]).into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Add,
                Box::new(Number(I(Uint::from_parts(vec![4903, 100]).into()))),
                Box::new(Number(I(Uint::from_parts(vec![u64::MAX, 100]).into())))
            )),
            Ok(N(I(Uint::from_parts(vec![4902, 201]).into())))
        );
    }

    #[test]
    fn sub() {
        assert_eq!(
            eval(BinaryOp(
                Subtract,
                Box::new(Number(I(Uint::from_parts(vec![0, 6]).into()))),
                Box::new(Number(I(Uint::from_parts(vec![5, 1]).into())))
            )),
            Ok(N(I(Uint::from_parts(vec![u64::MAX - 4, 4]).into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Subtract,
                Box::new(Number(I(Uint::from_parts(vec![u64::MAX, 1, 0]).into()))),
                Box::new(Number(I(Uint::from_parts(vec![0, 2]).into())))
            )),
            Ok(N(I(Sint::from(-1))))
        );
        assert_eq!(
            eval(BinaryOp(
                Subtract,
                Box::new(Number(I(10.into()))),
                Box::new(BinaryOp(
                    Add,
                    Box::new(Number(I(2.into()))),
                    Box::new(Number(I(5.into())))
                ))
            )),
            Ok(N(I(3.into())))
        );
    }

    #[test]
    fn mul() {
        assert_eq!(
            eval(BinaryOp(
                Add,
                Box::new(Number(I(10.into()))),
                Box::new(Identifier("x".to_string()))
            )),
            Err(Error::UndefinedIdentifier("x".to_string()))
        );
        assert_eq!(
            eval(BinaryOp(
                Multiply,
                Box::new(Number(I(20.into()))),
                Box::new(BinaryOp(
                    Subtract,
                    Box::new(Number(I(8.into()))),
                    Box::new(Number(I(3.into())))
                ))
            )),
            Ok(N(I(100.into())))
        );
    }

    #[test]
    fn div() {
        assert_eq!(
            eval(BinaryOp(
                Divide,
                Box::new(Number(I(10.into()))),
                Box::new(Number(I(2.into())))
            )),
            Ok(N(I(5.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Divide,
                Box::new(Number(I(125.into()))),
                Box::new(Number(I(3.into())))
            )),
            Ok(N(Fr(125.into(), 3.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Divide,
                Box::new(Number(I(10.into()))),
                Box::new(Number(I(0.into())))
            )),
            Err(Error::DivisionByZero)
        );
        assert_eq!(
            eval(BinaryOp(
                Divide,
                Box::new(Number(I(Uint::from_parts(vec![0, 3]).into()))),
                Box::new(Number(I(Uint::from_parts(vec![0, 1]).into())))
            )),
            Ok(N(I(3.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Divide,
                Box::new(Number(I(Uint::from_parts(vec![0, 0, 76]).into()))),
                Box::new(Number(I(Uint::from_parts(vec![2, 4]).into())))
            )),
            Ok(N(Fr(
                Uint::from_parts(vec![0, 0, 38]).into(),
                Uint::from_parts(vec![1, 2])
            )))
        );
    }

    #[test]
    fn modulo() {
        assert_eq!(
            eval(BinaryOp(
                Modulo,
                Box::new(Number(I(12.into()))),
                Box::new(Number(I(5.into())))
            )),
            Ok(N(I(2.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Modulo,
                Box::new(Number(I(12.into()))),
                Box::new(Number(I(3.into())))
            )),
            Ok(N(I(0.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Modulo,
                Box::new(Number(I(12.into()))),
                Box::new(Number(I(0.into())))
            )),
            Err(Error::DivisionByZero)
        );
        assert_eq!(
            eval(BinaryOp(
                Modulo,
                Box::new(Number(I(Uint::from_parts(vec![0, 39, 190, 0]).into()))),
                Box::new(Number(I(Uint::from_parts(vec![1, 2, 0]).into())))
            )),
            Ok(N(I(28.into())))
        );
    }

    #[test]
    fn pow() {
        assert_eq!(
            eval(BinaryOp(
                Pow,
                Box::new(Number(I(3.into()))),
                Box::new(Number(I(30.into())))
            )),
            Ok(N(I(205891132094649.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Pow,
                Box::new(Number(I(0.into()))),
                Box::new(Number(I(2.into())))
            )),
            Ok(N(I(0.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Pow,
                Box::new(Number(I(101.into()))),
                Box::new(Number(I(Uint::from_parts(vec![2, 0]).into())))
            )),
            Ok(N(I(10201.into())))
        );
        assert_eq!(
            eval(BinaryOp(
                Pow,
                Box::new(Number(I(20.into()))),
                Box::new(Number(I(Uint::from_parts(vec![2, 1]).into())))
            )),
            Err(Error::OutsideU64Range)
        );
        assert_eq!(
            eval(BinaryOp(
                Pow,
                Box::new(Number(I(Uint::from_parts(vec![1, 1]).into()))),
                Box::new(Number(I(3.into())))
            )),
            Ok(N(I(Uint::from_decimal(
                "6277101735386680764856636523970481806547819498980467802113"
            )
            .unwrap()
            .into())))
        );
    }

    #[test]
    fn shift() {
        assert_eq!(
            eval(BinaryOp(
                ShiftLeft,
                Box::new(Number(I(Uint::from_parts(vec![0, 33]).into()))),
                Box::new(Number(I(17.into())))
            )),
            Ok(N(I(Uint::from_parts(vec![0, 4325376]).into())))
        );
        assert_eq!(
            eval(BinaryOp(
                ShiftLeft,
                Box::new(Number(I(9043.into()))),
                Box::new(Number(I(Uint::from_decimal("89408901309823132819012")
                    .unwrap()
                    .into())))
            )),
            Err(Error::OutsideU64Range)
        );
        assert_eq!(
            eval(BinaryOp(
                ShiftRight,
                Box::new(Number(I(Uint::from_parts(vec![0, 102]).into()))),
                Box::new(Number(I(40.into())))
            )),
            Ok(N(I(Uint::from(1711276032).into())))
        );
        assert_eq!(
            eval(BinaryOp(
                ShiftRight,
                Box::new(Number(I(0.into()))),
                Box::new(Number(I(Uint::from_parts(vec![3, 93]).into())))
            )),
            Err(Error::OutsideU64Range)
        );
    }

    #[test]
    fn function() {
        assert_eq!(
            eval(Function(
                F::Pow,
                vec![Number(I(20.into())), Number(I(4.into()))]
            )),
            Ok(N(I(160000.into())))
        );
        assert_eq!(
            eval(Function(
                F::Pow,
                vec![
                    BinaryOp(
                        Pow,
                        Box::new(Number(I(2.into()))),
                        Box::new(Number(I(3.into())))
                    ),
                    Number(I(3.into()))
                ]
            )),
            Ok(N(I(512.into())))
        );
        assert_eq!(
            eval(Function(
                F::Pow,
                vec![
                    Number(I(10.into())),
                    Number(I(3.into())),
                    Number(I(1.into()))
                ]
            )),
            Ok(N(I(0.into())))
        );
        assert_eq!(
            eval(Function(
                F::Pow,
                vec![
                    Number(I(7.into())),
                    Number(I(1000.into())),
                    Number(I(65537.into()))
                ]
            )),
            Ok(N(I(33779.into())))
        );

        let rand = eval(Function(F::Random, vec![]))
            .unwrap()
            .into_uint()
            .unwrap();
        assert!(Uint::from(0) <= rand && rand <= Uint::from(u64::MAX));
        let rand = eval(Function(F::Random, vec![Number(I(128.into()))]))
            .unwrap()
            .into_uint()
            .unwrap();
        assert!(Uint::from(0) <= rand && rand < Uint::from(2).pow(128));

        assert_eq!(
            eval(Function(F::IsPrime, vec![Number(I(14929.into()))])),
            Ok(B(true))
        );
        assert_eq!(
            eval(Function(
                F::IsPrime,
                vec![Number(I(Uint::from_parts(vec![101, 2]).into()))]
            )),
            Ok(B(false))
        );

        assert_eq!(
            eval(Function(
                F::Modulo,
                vec![Number(I(500.into())), Number(I(7.into()))]
            )),
            Ok(N(I(3.into())))
        );
        assert_eq!(
            eval(Function(F::Modulo, vec![Number(I(500.into()))])),
            Err(Error::IncorrectFunctionArgumentCount)
        );
        assert_eq!(
            eval(Function(
                F::Random,
                vec![Number(I(10.into())), Number(I(20.into()))]
            )),
            Err(Error::IncorrectFunctionArgumentCount)
        );
        assert_eq!(
            eval(BinaryOp(
                Add,
                Box::new(Number(I(1.into()))),
                Box::new(Function(F::IsPrime, vec![Number(I(101.into()))]))
            )),
            Err(Error::IncorrectResultType)
        );
    }

    #[test]
    fn function_rsa() {
        assert!(matches!(
            eval(Function(F::RsaContext, vec![])),
            Ok(R::RsaContext(_))
        ));
        assert_eq!(
            eval(Function(F::RsaContext, vec![Number(I(1.into()))])),
            Err(Error::IncorrectFunctionArgumentCount)
        );

        let public = eval(Function(
            F::RsaContext,
            vec![Number(I(91.into())), Number(I(3.into()))],
        ))
        .unwrap();
        assert!(matches!(public, R::RsaContext(_)));
        let public: Expression = public.into();
        assert_eq!(
            eval(Function(
                F::RsaEncrypt,
                vec![public.clone(), Number(I(50.into()))]
            )),
            Ok(N(I(57.into())))
        );
        assert_eq!(
            eval(Function(
                F::RsaDecrypt,
                vec![public.clone(), Number(I(57.into()))]
            )),
            Err(Error::RsaError(RsaError::NoPrivateKey))
        );
        assert_eq!(
            eval(Function(F::RsaEncrypt, vec![public.clone()])),
            Err(Error::IncorrectFunctionArgumentCount)
        );
        assert_eq!(
            eval(Function(
                F::RsaDecrypt,
                vec![public.clone(), Number(I(10.into())), Number(I(5.into()))]
            )),
            Err(Error::IncorrectFunctionArgumentCount)
        );

        let private = eval(Function(
            F::RsaContext,
            vec![
                Number(I(323.into())),
                Number(I(5.into())),
                Number(I(173.into())),
            ],
        ))
        .unwrap();
        assert!(matches!(private, R::RsaContext(_)));
        let private: Expression = private.into();
        assert_eq!(
            eval(Function(
                F::RsaEncrypt,
                vec![private.clone(), Number(I(47.into()))]
            )),
            Ok(N(I(149.into())))
        );
        assert_eq!(
            eval(Function(
                F::RsaDecrypt,
                vec![private.clone(), Number(I(149.into()))]
            )),
            Ok(N(I(47.into())))
        );
        assert_eq!(
            eval(Function(
                F::RsaEncrypt,
                vec![private.clone(), public.clone()]
            )),
            Err(Error::IncorrectResultType)
        );
        assert_eq!(
            eval(Function(
                F::RsaDecrypt,
                vec![Number(I(6.into())), private.clone()]
            )),
            Err(Error::IncorrectResultType)
        );
    }

    #[test]
    fn evaluate_str() {
        assert_eq!(eval_str("3 - (2 + 1)"), N(I(Uint::from(0).into())));
        assert_eq!(
            eval_str("489302893 - (9210923 - 902) + (1029 + 89017961023)"),
            N(I(Uint::from(89498054924).into()))
        );
        assert_eq!(eval_str("680634015152086511092217113079 - 297443881838068275351068819490 - (104225356258694126196896705387 - 27381511251924541789634129124)"), N(I(Uint::from_parts(vec![14378934880222014286, 16607065565]).into())));
        assert_eq!(
            eval_str("947864135246 + 7894132165 * (4578453168 - 1238792)"),
            N(I(Uint::from_parts(vec![17686392105836587670, 1]).into()))
        );
        assert!(catch_unwind(|| eval_str("10 - 12")).is_ok());
        assert!(catch_unwind(|| eval_str("x + 2 * y")).is_err());
        assert_eq!(
            eval_str("(2 + 3) ^ 10 * 2 - 123"),
            N(I(Uint::from(19531127).into()))
        );
        assert!(catch_unwind(|| eval_str("3 * 2 ^ 4983208112789322158512313565")).is_err());
        assert_eq!(
            eval_str("7 % 5 + 2 * (68490 - 1) ^ 10 % 3129"),
            N(I(Uint::from(571).into()))
        );
        assert_eq!(
            eval_str("(8 * 493082904) % 890183902183983442748987853"),
            N(I(Uint::from(3944663232).into()))
        );
        assert!(catch_unwind(|| eval_str("0 / 0")).is_err());
        assert_eq!(
            eval_str("{7 ** 48} % 2 ** 64 / 53"),
            N(Fr(6243698426513668225.into(), 53.into()))
        );
        assert_eq!(
            eval_str("(15 + 727) * (2 ** 30) >> 25"),
            N(I(Uint::from(23744).into()))
        );
        assert_eq!(
            eval_str("(76 << 179032) >> 179031"),
            N(I(Uint::from(152).into()))
        );
        assert_eq!(
            eval_str("pow { 1001, 13 ^ 4, 1 << 128 }"),
            N(I(Uint::from_parts(vec![
                13369082835333234793,
                2800281300878819940
            ])
            .into()))
        );
        assert_eq!(eval_str("isprime\t[ 2038074743 ]"), B(true));
        assert_eq!(
            eval_str("rsa_encrypt rsa_gen(25021, 101), 1438"),
            N(I(4476.into()))
        );
    }
}
