use crate::algs;
use crate::bigint::Uint;
use rand::{thread_rng, Rng};
use std::convert::TryInto;

/// Describes the desired characteristics of a number to be generated.
///
/// * `size` - Describes how big the number should be.
/// * `rng` - If `None`, the default `thread_rng()` will be used. In order to get deterministic
/// results, supply your own random number generator.
/// * `prime` - Determines whether the number is prime.
#[derive(Debug)]
pub struct RandomOptions<'a, R: Rng> {
    rng: &'a mut R,
    size: RandomSize<'a>,
    prime: bool,
}

impl<R: Rng> RandomOptions<'_, R> {
    /// Creates a random `Uint` from this set of options.
    ///
    /// `opts.gen()` is equivalent to `random_uint(opts)`.
    pub fn gen(self) -> Uint {
        random_uint(self)
    }
}

/// Builder used to create `RandomOptions` instances.
/// It owns a random number generator which is passed to `random_uint` alongside the options.
#[derive(Debug)]
pub struct RandomOptionsBuilder<R: Rng> {
    rng: R,
    prime: bool,
}

impl Default for RandomOptionsBuilder<rand::rngs::ThreadRng> {
    fn default() -> Self {
        RandomOptionsBuilder {
            rng: thread_rng(),
            prime: false,
        }
    }
}

impl RandomOptionsBuilder<rand::rngs::ThreadRng> {
    /// Creates a new `RandomOptionsBuilder` using the thread-local random number generator.
    pub fn new() -> Self {
        Default::default()
    }
}

impl<R: Rng> RandomOptionsBuilder<R> {
    /// Creates a new `RandomOptionsBuilder` using the given `rng`.
    pub fn deterministic(rng: R) -> Self {
        RandomOptionsBuilder { rng, prime: false }
    }

    /// Request generated numbers to be prime.
    pub fn prime(self) -> Self {
        RandomOptionsBuilder {
            prime: true,
            ..self
        }
    }

    /// Cancel request for generated numbers to be prime.
    /// Note this does not guarantee the numbers will be composite.
    pub fn not_prime(self) -> Self {
        RandomOptionsBuilder {
            prime: false,
            ..self
        }
    }

    /// Generates `RandomOptions` describing a number with the supplied `size`.
    pub fn with_size<'a>(&'a mut self, size: RandomSize<'a>) -> RandomOptions<'a, R> {
        RandomOptions {
            rng: &mut self.rng,
            size,
            prime: self.prime,
        }
    }

    /// Generates `RandomOptions` with `RandomSize::Bits { n, exact: false }`.
    pub fn bits(&mut self, n: usize) -> RandomOptions<'_, R> {
        self.with_size(RandomSize::Bits { n, exact: false })
    }

    /// Generates `RandomOptions` with `RandomSize::Bits { n, exact: true }`.
    pub fn bits_exact(&mut self, n: usize) -> RandomOptions<'_, R> {
        self.with_size(RandomSize::Bits { n, exact: true })
    }

    /// Genreates `RandomOptions` with `RandomSize::Range { a, b }`.
    pub fn range<'a>(&'a mut self, a: &'a Uint, b: &'a Uint) -> RandomOptions<'a, R> {
        self.with_size(RandomSize::Range { a, b })
    }
}

/// Describes several available metrics for specifying how big the generated number `x` should be.
///
/// # Variants
///
/// * `Bits`: If `exact`, `x` will have `n` bits with the highest bit being set. This means that it
/// will have `n-1` bits of randomness. If `!exact`, all `n` bits will be random.
/// * `Range`: `a <= x < b`
#[derive(Debug, Clone)]
pub enum RandomSize<'a> {
    Bits { n: usize, exact: bool },
    Range { a: &'a Uint, b: &'a Uint },
}

/// Generates a random number described by `opts`.
/// Look at the documentation of `RandomOptions` for different methods numbers can be generated.
pub fn random_uint<R: Rng>(opts: RandomOptions<R>) -> Uint {
    let rng = opts.rng;

    loop {
        let mut number = match opts.size {
            RandomSize::Bits { n, exact } => {
                let mut data = Vec::<u64>::new();
                data.resize((n + 63) / 64, 0);
                rng.fill(&mut data[..]);

                let last_index = (n + 63) / 64 - 1;
                let last_part_bits = n % 64;
                if last_part_bits != 0 {
                    data[last_index] &= (1 << last_part_bits) - 1;
                }

                let mut x = Uint::from_parts(data);
                if exact {
                    x.set_bit(n - 1, true);
                }
                x
            }

            RandomSize::Range { a, b } => loop {
                let diff = b.clone() - a;
                let cand = random_uint(RandomOptions {
                    rng,
                    size: RandomSize::Bits {
                        n: diff.bits(),
                        exact: false,
                    },
                    prime: false,
                }) + a;

                if a <= &cand && &cand < b {
                    return cand;
                }
            },
        };

        if !opts.prime {
            return number;
        } else {
            number.set_bit(0, true);
            if is_prime(&number) {
                return number;
            }
        }
    }
}

#[cfg(test)]
mod random_tests {
    use crate::bigint::Uint;
    use crate::crypto::{is_prime, random_uint, RandomOptionsBuilder};
    use rand_chacha::{rand_core::SeedableRng, ChaChaRng};
    use std::convert::TryInto;

    static TEST_BIT_LENGTHS: [usize; 10] = [1, 2, 3, 32, 64, 100, 128, 200, 4096, 6000];

    #[test]
    fn deterministic() {
        let mut opts = RandomOptionsBuilder::deterministic(ChaChaRng::from_seed([
            2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0,
        ]));
        assert_eq!(
            random_uint(opts.bits(100)),
            Uint::from_parts(vec![4142360511309225834, 42769914178])
        );
        assert_eq!(
            random_uint(opts.bits(100)),
            Uint::from_parts(vec![12218006855304899749, 11828931015])
        );
        assert_eq!(
            random_uint(opts.bits(100)),
            Uint::from_parts(vec![6615093528791545015, 18083172371])
        );

        let mut opts = RandomOptionsBuilder::deterministic(ChaChaRng::from_seed([
            3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0,
        ]));
        assert_eq!(
            random_uint(opts.bits(100)),
            Uint::from_parts(vec![16185336815585874304, 43632052869])
        );
        assert_eq!(
            random_uint(opts.bits(100)),
            Uint::from_parts(vec![6018681190734534718, 53260504980])
        );
        assert_eq!(
            random_uint(opts.bits(100)),
            Uint::from_parts(vec![3690421832627292909, 3177590036])
        );
    }

    #[test]
    fn range() {
        let mut opts = RandomOptionsBuilder::new();
        for b in TEST_BIT_LENGTHS.iter().copied() {
            for _ in 0..100 {
                let x = random_uint(opts.bits_exact(b));
                let min = Uint::from(1) << (b - 1);
                let max = Uint::from(1) << b;
                assert!(
                    min <= x && x < max,
                    "bits: {}, random_bits_exact(): {:?}, min: {:?}, max: {:?}",
                    b,
                    x,
                    min,
                    max
                );

                let x = random_uint(opts.bits(b));
                assert!(
                    x < max,
                    "bits: {}, random_bits(): {:?}, max: {:?}",
                    b,
                    x,
                    max
                );
            }
        }
    }

    #[test]
    fn bit_distribution() {
        // Performs z-value test described in (https://en.wikipedia.org/wiki/Binomial_test#Large_samples)
        // The z-value threshold is chosen such that the probability that all ~10500 tests pass is 99%
        const TEST_POPULATION: u32 = 500;
        const Z_THRESHOLD: f64 = 4.9;

        let mut opts = RandomOptionsBuilder::new();

        for bits in TEST_BIT_LENGTHS.iter().copied() {
            let mut bit_count = Vec::new();
            bit_count.resize(bits, 0);

            for _ in 0..TEST_POPULATION {
                let x = random_uint(opts.bits(bits));
                for i in 0..bits {
                    bit_count[i] += x.bit(i) as u32;
                }
            }

            for c in bit_count.iter() {
                let z = (*c as f64 - TEST_POPULATION as f64 / 2.)
                    / (TEST_POPULATION as f64 / 4.).sqrt();
                assert!(
                    z.abs() < Z_THRESHOLD,
                    "bits: {}, z-value: {}, count: {}/{}",
                    bits,
                    z,
                    c,
                    TEST_POPULATION - c
                );
            }
        }
    }

    #[test]
    fn uniform_distribution() {
        // The z-value threshold is chosen such that the probability that all 1000 tests pass is 99%
        const TEST_POPULATION: u32 = 100000;
        const Z_THRESHOLD: f64 = 4.416;
        let a: Uint = Uint::from_parts(vec![1000, 1]);
        let b: Uint = Uint::from_parts(vec![2000, 1]);
        const DIFF: usize = 1000;
        const PROB: f64 = 1. / DIFF as f64;
        assert_eq!((b.clone() - a.clone()).try_into(), Ok(DIFF as u64));

        let mut counts = Vec::new();
        counts.resize(DIFF, 0);

        let mut opts = RandomOptionsBuilder::new();
        for _ in 0..TEST_POPULATION {
            let x = opts.range(&a, &b).gen();
            assert!(a <= x && x < b);
            counts[TryInto::<u64>::try_into(x - &a).unwrap() as usize] += 1;
        }

        for c in counts.iter() {
            let z = (*c as f64 - TEST_POPULATION as f64 * PROB)
                / (TEST_POPULATION as f64 * PROB * (1. - PROB)).sqrt();
            assert!(z.abs() < Z_THRESHOLD, "z-value: {}, count: {}", z, c);
        }
    }

    #[test]
    fn prime() {
        let mut opts = RandomOptionsBuilder::new().prime();
        let prime_lengths = if cfg!(debug_assertions) {
            [8, 32, 64, 100].iter()
        } else {
            [8, 32, 64, 100, 128, 200, 384, 497].iter()
            // taking too long [8, 32, 64, 100, 128, 200, 384, 497, 1024].iter()
        };
        for b in prime_lengths.copied() {
            for _ in 0..1 {
                let x = opts.bits_exact(b).gen();
                let min = Uint::from(1) << (b - 1);
                let max = Uint::from(1) << b;
                assert!(min <= x && x < max);
                assert!(is_prime(&x));
            }
        }
    }
}

/// Checks whether `x` is a prime number.
///
/// # Implementation
///
/// Initially, divisibily by a set of small prime numbers is checked.
/// If `x` is small, a naive algorithm checking division by every odd number is used.
/// For a large `x`, a probabilistic Miller-Rabin test is performed with 32 rounds.
pub fn is_prime(x: &Uint) -> bool {
    if x == &0 || x == &1 {
        return false;
    }
    if is_divisble_by_small_prime(x) {
        return false;
    }

    if x < &Uint::from(1 << 32) {
        is_prime_naive(x.try_into().unwrap())
    } else {
        !is_composite_miller_rabin(x.clone(), 32)
    }
}

const SMALL_PRIMES: [u64; 32] = [
    2, 3, 5, 7, 11, 13, 17, 19, 23, 27, 29, 31, 37, 41, 43, 47, 53, 49, 61, 67, 71, 73, 79, 83, 89,
    97, 101, 103, 107, 109, 113, 127,
];

/// Checks if `x` is divisble by any of the first 32 prime numbers.
fn is_divisble_by_small_prime(x: &Uint) -> bool {
    let value = x.try_into().unwrap_or(u64::MAX);
    for p in SMALL_PRIMES.iter() {
        if p * p > value {
            return false;
        }
        if x % *p == 0 {
            return true;
        }
    }
    false
}

/// Checks naively whether `x` is prime by checking divisibility by odd numbers greater than
/// `SMALL_PRIMES`. You need to check with `is_divisble_by_small_prime` to avoid missing small
/// primes.
fn is_prime_naive(x: u64) -> bool {
    let mut n = SMALL_PRIMES[SMALL_PRIMES.len() - 1] + 2;
    while n * n <= x {
        if x % n == 0 {
            return false;
        }
        n += 2;
    }
    true
}

/// Performs `k` passes of the probabilistic Miller-Rabin primality test.
/// Returns `true` if `n` is definitely composite.
///
/// Implementation based on [https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test].
/// Variable names correspond to names in the article, so they are quite short.
#[allow(clippy::many_single_char_names)]
fn is_composite_miller_rabin(n: Uint, k: u32) -> bool {
    // The test does not work for two. This should have already been detected using
    // `is_divisble_by_small_prime`.
    assert!(n >= Uint::from(3));

    let (r, d) = {
        let mut n = n.clone() - 1;
        let mut r = 0;
        while !n.bit(0) {
            // divisble by 2
            n = n >> 1;
            r += 1;
        }
        (r, n)
    };

    let two = Uint::from(2);
    let n_minus_one = n.clone() - 1;

    let mut opts = RandomOptionsBuilder::new();
    'witness: for _ in 0..k {
        let a = opts.range(&two, &n_minus_one).gen();
        let mut x = a.pow_mod(&d, &n);
        if x == 1 || x == n_minus_one {
            continue 'witness;
        }
        for _ in 0..(r - 1) {
            x = (x.clone() * x) % &n;
            if x == n_minus_one {
                continue 'witness;
            }
        }
        return true;
    }

    false
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Rsa {
    private_key: Option<Uint>,
    exponent: u64,
    modulus: Uint,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum RsaError {
    NonPrimeExponent,
    MessageTooLong,
    NoPrivateKey,
}

impl Rsa {
    /// Generates an RSA context with a random modulus of length of `bits` and a private key
    /// corresponding to the given `exponent`.
    /// This context is suitable for both encryption and decryption.
    ///
    /// # Errors
    ///
    /// * `RsaError::NonPrimeExponent` if the exponent is not invertible in the randomly generated
    /// modulus. You should prevent this by using a prime exponent.
    pub fn private_gen(bits: usize, exponent: u64) -> Result<Self, RsaError> {
        // Based on [https://en.wikipedia.org/wiki/RSA_(cryptosystem)]
        let p = RandomOptionsBuilder::new()
            .prime()
            .bits_exact(bits / 2)
            .gen();
        let q = RandomOptionsBuilder::new()
            .prime()
            .bits_exact(bits / 2)
            .gen();
        // TODO test whether the generated primes are too close and suscpetible to Fermat factorization
        let totient = (p.clone() - 1) * (q.clone() - 1);
        let modulus = p * q;
        let d = algs::modular_multiplicative_inverse(Uint::from(exponent), totient)
            .ok_or(RsaError::NonPrimeExponent)?;

        Ok(Self {
            private_key: Some(d),
            exponent,
            modulus,
        })
    }

    /// Generates an RSA context with the given `modulus`, `exponent`, and `private_key`.
    /// This function has no way to verify that the private key is valid for the given modulus and exponent.
    /// If the values provided are indeed correct, the context is suitable for both encryption and decryption.
    pub fn private(modulus: Uint, exponent: u64, private_key: Uint) -> Self {
        Self {
            private_key: Some(private_key),
            modulus,
            exponent,
        }
    }

    /// Generates an RSA context with the given `modulus` and `exponent`.
    /// This context is **only suitable for encryption**, since it does not have a private key.
    pub fn public(modulus: Uint, exponent: u64) -> Self {
        Self {
            private_key: None,
            exponent,
            modulus,
        }
    }

    /// Encrypts the given message with the public key related to this context.
    ///
    /// # Errors
    ///
    /// * `RsaError::MessageTooLong` if the message is longer than the modulus.
    pub fn encrypt<T>(&self, message: T) -> Result<T, RsaError>
    where
        T: From<Uint> + Into<Uint>,
    {
        let message = message.into();
        if message.bits() <= self.modulus.bits() {
            Ok(message
                .pow_mod(&Uint::from(self.exponent), &self.modulus)
                .into())
        } else {
            Err(RsaError::MessageTooLong)
        }
    }

    /// Decrypts the given message with the private related to this context.
    ///
    /// # Errors
    ///
    /// * `RsaError::MessageTooLong` if the message is longer than the modulus.
    /// * `RsaError::NoPrivateKey` if this context does not have a private key and is therefore not
    /// suitable for decryption.
    pub fn decrypt<T>(&self, message: T) -> Result<T, RsaError>
    where
        T: From<Uint> + Into<Uint>,
    {
        let message = message.into();
        if message.bits() <= self.modulus.bits() {
            Ok(message
                .pow_mod(
                    self.private_key.as_ref().ok_or(RsaError::NoPrivateKey)?,
                    &self.modulus,
                )
                .into())
        } else {
            Err(RsaError::MessageTooLong)
        }
    }

    pub fn get_modulus(&self) -> &Uint {
        &self.modulus
    }

    pub fn get_exponent(&self) -> u64 {
        self.exponent
    }

    pub fn get_private_key(&self) -> &Option<Uint> {
        &self.private_key
    }
}

impl Default for Rsa {
    /// **WARNING**: This default bit-length of 512 bits is insecure and will be increased in the
    /// future when the system can handle it reliably.
    fn default() -> Self {
        Self::private_gen(if cfg!(debug_assertions) { 128 } else { 512 }, 65537).unwrap()
    }
}

#[cfg(test)]
mod number_theory_tests {
    use crate::bigint::Uint;
    use crate::crypto::{is_prime, RandomOptionsBuilder, Rsa, RsaError};

    #[test]
    fn prime() {
        assert!(!is_prime(&0.into()));
        assert!(!is_prime(&1.into()));
        assert!(is_prime(&2.into()));
        assert!(is_prime(&3.into()));
        assert!(is_prime(&101.into()));
        assert!(!is_prime(&150.into()));
        assert!(!is_prime(&1001.into()));
        assert!(!is_prime(&43039.into()));
        assert!(is_prime(&5591.into()));
        assert!(is_prime(&887677523.into()));
        assert!(!is_prime(&192363751.into()));
        assert!(!is_prime(&1911045041.into()));

        assert!(!is_prime(&Uint::from_parts(vec![1, 2])));

        if !cfg!(debug_assertions) {
            assert!(is_prime(
                &Uint::from_decimal(
                    "46684199631140216676828059575106103835247711636100630803759115451630068172657"
                )
                .unwrap()
            ));
            assert!(!is_prime(
                &Uint::from_decimal(
                    "28213932031852417792893224909764922325277866151300685168939815520360138778557"
                )
                .unwrap()
            ));
            assert!(is_prime(&Uint::from_decimal("175328935694292546311736740935748093111501891785681140148894712971386629408645621330424127644728623479643633008137450537209460367000821535374071953391142832994082966547541961265305015928060496093279526764951918056219514066916282995494200202292879168417088221030736918726087433007275431949302820697632365947871").unwrap()));
        }
        // These tests work but take an absurd amount of time: TODO div_rem speed needs to be improved
        //assert!(!is_prime(&Uint::from_decimal("10537032678491034976980463467373943344808497722640473822484367197240891678802742195066451052115762472753618235549975407737563266770667008786603746580469873244767582300112485942122363078322741242263573194094728462045766518386476353926223280044772548198013562340728095041759382186956813122369794339712183974500060744416282437485386805569853519062730372405658456147086997494309964091367253707674813379535140092865215829413892379451078906458086066997360364157278072203369010043806900586443038670327692595027224028705520274293748299096108306478581158989983051676486632245975339937299631364415813434452211712036139425878329").unwrap()));
        //assert!(is_prime(&Uint::from_decimal("10449636387581391924186070679439272969251167551148048270443978514132518277805200369165940478096250129881218529377001").unwrap()));
    }

    #[test]
    fn rsa() {
        let private_rsa = Rsa::default();
        let bits = private_rsa.get_modulus().bits();

        let mut rng = RandomOptionsBuilder::new();
        for _ in 0..10 {
            let a = rng.bits(bits - 1).gen();
            let e: Uint = private_rsa.encrypt(a.clone()).unwrap();
            let d: Uint = private_rsa.decrypt(e).unwrap();
            assert_eq!(a, d);

            let r = rng.bits_exact(bits + 1).gen();
            assert_eq!(
                private_rsa.encrypt(r.clone()),
                Err(RsaError::MessageTooLong)
            );
            assert_eq!(private_rsa.decrypt(r), Err(RsaError::MessageTooLong));
        }

        let public_rsa = Rsa::public(Uint::from(946428221983104081), 17);
        assert_eq!(
            public_rsa.encrypt(Uint::from(14889)),
            Ok(Uint::from(96558661145021670))
        );
        assert_eq!(
            public_rsa.decrypt(Uint::from(96558661145021670)),
            Err(RsaError::NoPrivateKey)
        );
    }
}
