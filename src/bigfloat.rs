use crate::bigint::{Sint, Uint};
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Debug, Clone, PartialEq, Eq)]
#[non_exhaustive]
/// A floating point number with a constant precision P.
///
/// # Precision
///
/// Operation can only be performed on Floats with the same precision, which is statically
/// enforced by the generic parameter.
/// In order to perform operations on operands with different precisions, call `set_precision`
/// earlier.
///
/// # Rounding
///
/// Rounding occurs by default in all operations. Ties are always broken away from zero (right now
/// this behavior is not configurable).
pub enum Float<const P: usize> {
    Zero,
    Value(Value),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Value {
    mantissa: Sint,
    exponent: i64,
}

impl<const P: usize> Float<P> {
    /// Sets the precision of this `Float`.
    /// If the precision is decreased, the additional bits are rounded, breaking ties away from zero.
    ///
    /// Even if the current `Float` is invalid, this function will correctly set the precision, so it
    /// can be used to fix invalid instances.
    pub fn set_precision<const N: usize>(self) -> Float<N> {
        match self {
            Float::Zero => Float::Zero,
            Float::Value(Value { mantissa, exponent }) => {
                // Using B instead of P allows this function to fix invalid values. For a valid value, B == P.
                #[allow(non_snake_case)]
                let B = mantissa.bits();

                let mut exponent = exponent + B as i64 - N as i64;
                let mantissa = match N.cmp(&B) {
                    Ordering::Equal => mantissa,
                    Ordering::Greater => mantissa << (N - B),
                    Ordering::Less => {
                        // Due to the way Shr is implemented on Sint (rounding towards zero, not -inf),
                        // if the number in question is negative, we need to subtract one instead of adding
                        // to get the correctly rounded result.
                        let tie_breaker = if mantissa.le(&0) { -1 } else { 1 };
                        let mut rounded = ((mantissa >> (B - N - 1)) + tie_breaker) >> 1;
                        if rounded.bits() > N {
                            // If rounding produced extra bits, we want to ignore them, not round again.
                            // TODO Add more test cases to ensure this behaviour is correct.
                            rounded = rounded >> 1;
                            exponent += 1;
                        }
                        rounded
                    }
                };

                Float::Value(Value { exponent, mantissa }).verified()
            }
        }
    }

    /// Takes `self` to the power of `exponent`.
    ///
    /// # Panics
    ///
    /// Panics when attempting to evaluate `0^0`.
    /// Also panics if the internal `i64` exponent overflows.
    pub fn pow_u64(self, mut exponent: u64) -> Self {
        assert!(!(self.is_zero() && exponent == 0));

        let mut cur = self;
        let mut product = Float::from(1.0).set_precision::<P>();
        while exponent > 0 {
            println!("product {:?}, cur {:?}", product, cur);
            if exponent & 1 == 1 {
                product = product * &cur;
            }
            exponent >>= 1;
            cur = cur.clone() * cur;
        }
        product
    }

    /// Creates new `Float` with a value of zero.
    pub fn zero() -> Self {
        Float::Zero
    }

    /// Creates new `Float` with a value equal to `mantissa * 2^exponent` which has the given `precision`.
    ///
    /// `mantissa` cannot be equal to zero. If you want to create a `Float` equal to 0, use
    /// `Float::zero()` instead.
    ///
    /// # Panics
    ///
    /// The mantissa must have exactly `presision` significant bits and be non-zero.
    pub fn from_parts(mantissa: Sint, exponent: i64) -> Self {
        Float::Value(Value { mantissa, exponent }).verified()
    }

    /// Returns whether this `Float` is equal to zero.
    pub fn is_zero(&self) -> bool {
        matches!(self, Float::Zero)
    }

    /// Internal function for verifying the invariants needed a `Float`.
    /// Panics if any invariant is not unheld.
    ///
    /// > Value
    /// - `mantissa != 0`
    /// - `mantissa.bits() == precision`
    #[track_caller]
    fn verify_invariants(&self) {
        match self {
            Float::Zero => (),
            Float::Value(Value { mantissa, .. }) => {
                assert!(!mantissa.eq(&0));
                assert!(mantissa.bits() == P);
            }
        }
    }

    /// Convenience function to verify invariants without introducing unnecessary variables.
    #[track_caller]
    fn verified(self) -> Self {
        self.verify_invariants();
        self
    }
}

impl<const P: usize> From<Uint> for Float<P> {
    fn from(value: Uint) -> Float<P> {
        Sint::from(value).into()
    }
}

impl<const P: usize> From<Sint> for Float<P> {
    fn from(value: Sint) -> Float<P> {
        if value.eq(&0) {
            Float::Zero
        } else {
            Float::<P>::Value(Value {
                mantissa: value,
                exponent: 0,
            })
            .set_precision()
        }
    }
}

impl From<f64> for Float<53> {
    fn from(value: f64) -> Float<53> {
        if value == 0f64 {
            return Float::zero();
        }

        assert!(value.is_normal());
        let value = value.to_bits();
        let sign = value & (1 << 63) > 0;

        let exp = ((value >> 52) & ((1 << 11) - 1)) as i64 - 1023;
        let mantissa = 1 << 52 | value & ((1 << 52) - 1);

        Float::Value(Value {
            mantissa: Sint::from_parts(Uint::from(mantissa), sign),
            exponent: exp - 52,
        })
        .verified()
    }
}

impl<const P: usize> PartialOrd for Float<P> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<const P: usize> Ord for Float<P> {
    fn cmp(&self, other: &Self) -> Ordering {
        let (a, b) = match (self, other) {
            (Float::Zero, Float::Zero) => return Ordering::Equal,
            (Float::Value(Value { mantissa, .. }), Float::Zero) => return mantissa.partial_cmp(&0).unwrap(),
            (Float::Zero, Float::Value(Value { mantissa, .. })) => {
                return mantissa.partial_cmp(&0).unwrap().reverse()
            }
            (Float::Value(a), Float::Value(b)) => (a, b),
        };

        let reverse = match (a.mantissa.le(&0), b.mantissa.le(&0)) {
            (false, true) => return Ordering::Greater,
            (true, false) => return Ordering::Less,
            (true, true) => true,
            (false, false) => false,
        };

        let cmp = a
            .exponent
            .cmp(&b.exponent)
            .then_with(|| a.mantissa.abs.cmp(&b.mantissa.abs));
        if reverse {
            cmp.reverse()
        } else {
            cmp
        }
    }
}

impl<const P: usize> Add<&Float<P>> for Float<P> {
    type Output = Float<P>;
    fn add(self, other: &Float<P>) -> Float<P> {
        self + other.clone()
    }
}

impl<const P: usize> Add<Float<P>> for Float<P> {
    type Output = Float<P>;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn add(self, other: Float<P>) -> Float<P> {
        match (self, other) {
            (Float::Zero, Float::Zero) => Float::Zero,
            (Float::Zero, f @ Float::Value(_)) | (f @ Float::Value(_), Float::Zero) => f,
            (Float::Value(a), Float::Value(b)) => {
                let (mut large, small) = if a.exponent >= b.exponent {
                    (a, b)
                } else {
                    (b, a)
                };

                large.mantissa =
                    (large.mantissa << (large.exponent - small.exponent) as usize) + small.mantissa;
                large.exponent = small.exponent;
                if large.mantissa == 0 {
                    Float::Zero
                } else {
                    Float::<P>::Value(large).set_precision()
                }
            }
        }
    }
}

impl<const P: usize> Sub<&Float<P>> for Float<P> {
    type Output = Float<P>;
    fn sub(self, other: &Float<P>) -> Float<P> {
        self - other.clone()
    }
}

impl<const P: usize> Sub<Float<P>> for Float<P> {
    type Output = Float<P>;
    fn sub(self, other: Float<P>) -> Float<P> {
        self + (-other)
    }
}

impl<const P: usize> Neg for Float<P> {
    type Output = Float<P>;
    fn neg(self) -> Float<P> {
        match self {
            Float::Zero => Float::Zero,
            Float::Value(Value { mantissa, exponent }) => Float::Value(Value {
                mantissa: -mantissa,
                exponent: exponent,
            }),
        }
    }
}

impl<const P: usize> Mul<&Float<P>> for Float<P> {
    type Output = Float<P>;
    fn mul(self, other: &Float<P>) -> Float<P> {
        self * other.clone()
    }
}

impl<const P: usize> Mul<Float<P>> for Float<P> {
    type Output = Float<P>;

    /// Panics if the exponent overflows `i64`.
    fn mul(self, other: Float<P>) -> Float<P> {
        match (self, other) {
            (Float::Zero, _) | (_, Float::Zero) => Float::Zero,
            (Float::Value(a), Float::Value(b)) => Float::<P>::Value(Value {
                mantissa: a.mantissa * b.mantissa,
                // TODO Check if other operations can overflow the exponent and explicitly protect
                // against it.
                exponent: a
                    .exponent
                    .checked_add(b.exponent)
                    .expect("Float exponent overflowed"),
            })
            .set_precision(),
        }
    }
}

impl<const P: usize> Div<&Float<P>> for Float<P> {
    type Output = Float<P>;
    fn div(self, other: &Float<P>) -> Float<P> {
        self / other.clone()
    }
}

impl<const P: usize> Div<Float<P>> for Float<P> {
    type Output = Float<P>;

    fn div(self, other: Float<P>) -> Float<P> {
        match (self, other) {
            (_, Float::Zero) => panic!("Float division by zero"),
            (Float::Zero, _) => Float::Zero,
            (Float::Value(a), Float::Value(b)) => Float::<P>::Value(Value {
                // Shifting numerator by P bits in order to maintain that precision
                // TODO decide whether there is a more preformant way to do this (right now these
                // calculations are done in 2P precision)
                mantissa: (a.mantissa << P) / b.mantissa,
                exponent: a.exponent - b.exponent - P as i64,
            })
            .set_precision(),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{bigfloat::Float, bigint::Sint};
    use std::cmp::Ordering;
    use std::panic::catch_unwind;

    #[test]
    fn cmp() {
        let zero1 = Float::from(0.0);
        let zero2 = Float::zero();
        let zero3 = Float::from(-0.0);
        assert_eq!(zero1, zero2);
        assert_eq!(zero1, zero3);

        let one1 = Float::from(1.0);
        let one2 = Float::from_parts(Sint::from(4 << 50), -52);
        assert_eq!(one1, one2);

        let quarter1 = Float::from(0.25);
        let quarter2 = Float::<2>::from_parts(Sint::from(2), -3).set_precision::<53>();
        assert_eq!(quarter1, quarter2);

        let frac1 = Float::from(1. / 3.);
        let frac2 = Float::from(2. / 5.);
        assert_ne!(frac1, frac2);

        let five = Float::<5>::from_parts(Sint::from(20), -2).set_precision::<53>();

        assert_eq!(one1.cmp(&quarter1), Ordering::Greater);
        assert_eq!(quarter1.cmp(&one2), Ordering::Less);
        assert_eq!(quarter1.cmp(&quarter2), Ordering::Equal);
        assert_eq!(quarter1.cmp(&zero1), Ordering::Greater);
        assert_eq!(zero2.cmp(&quarter1), Ordering::Less);

        let neg = Float::<3>::from_parts(Sint::from(-6), 0).set_precision::<53>();
        let neg_close = Float::<4>::from_parts(Sint::from(-14), -1).set_precision::<53>();
        let more_neg = Float::<4>::from_parts(Sint::from(-10), 2).set_precision::<53>();

        assert_eq!(frac1.cmp(&frac2), Ordering::Less);
        assert_eq!(frac2.cmp(&frac1), Ordering::Greater);

        assert_eq!(five.cmp(&quarter1), Ordering::Greater);
        assert_eq!(frac2.cmp(&five), Ordering::Less);

        assert_eq!(neg.cmp(&zero2), Ordering::Less);
        assert_eq!(neg.cmp(&frac1), Ordering::Less);
        assert_eq!(neg_close.cmp(&quarter2), Ordering::Less);
        assert_eq!(five.cmp(&neg), Ordering::Greater);

        assert_eq!(neg_close.cmp(&neg), Ordering::Less);
        assert_eq!(neg.cmp(&neg_close), Ordering::Greater);
        assert_eq!(more_neg.cmp(&neg_close), Ordering::Less);
        assert_eq!(neg_close.cmp(&more_neg), Ordering::Greater);
    }

    #[test]
    fn from() {
        assert_eq!(
            Float::from(1.0),
            Float::from_parts(Sint::from(1 << 52), -52)
        );
        assert_eq!(
            Float::from(3.0),
            Float::from_parts(Sint::from(3 << 51), -51)
        );
        assert_eq!(
            Float::from(0.125),
            Float::from_parts(Sint::from(1 << 52), -55)
        );

        Float::from(89043829043.190283).verify_invariants();
        Float::from(0.).verify_invariants();
        Float::from(-2.3).verify_invariants();

        assert!(catch_unwind(|| Float::<2>::from_parts(Sint::from(5), 10)).is_err());
        assert!(catch_unwind(|| Float::<3>::from_parts(Sint::from(5), 10)).is_ok());
        assert!(catch_unwind(|| Float::<4>::from_parts(Sint::from(5), 10)).is_err());
        assert!(catch_unwind(|| Float::from(f64::NAN)).is_err());

        assert_eq!(
            Float::from((1u64 << 32) as f64 + 1.5),
            Float::<34>::from_parts(Sint::from((1 << 33) + 3), -1).set_precision::<53>()
        );

        assert_eq!(
            Float::<10>::from(Sint::from(100)),
            Float::from_parts(Sint::from(800), -3)
        );
        assert_eq!(
            Float::<3>::from(Sint::from(-240)),
            Float::from_parts(Sint::from(-4), 6)
        );
    }

    #[test]
    fn set_precision() {
        for x in [
            0.,
            1.,
            1.3,
            1. / 3.,
            47839.0,
            1000.0001,
            84903.901,
            -1.3,
            12.0,
        ]
        .iter()
        .copied()
        {
            let f1 = Float::from(x);
            let f2 = Float::from(x).set_precision::<2030>();
            let f3 = Float::from(x).set_precision::<673>();
            f1.verify_invariants();
            f2.verify_invariants();
            f3.verify_invariants();
        }

        assert_eq!(
            Float::from(4.5).set_precision::<2>(),
            Float::from_parts(Sint::from(2), 1)
        );
        assert_eq!(
            Float::from(5.0).set_precision::<2>(),
            Float::from_parts(Sint::from(3), 1)
        );
        assert_eq!(
            Float::from(6.0).set_precision::<2>(),
            Float::from_parts(Sint::from(3), 1)
        );
        assert_eq!(
            Float::from(7.0).set_precision::<2>(),
            Float::from_parts(Sint::from(2), 2)
        );
        assert_eq!(
            Float::from(1.0 / 3.0).set_precision::<32>(),
            Float::from_parts(Sint::from(2863311531), -33)
        );
    }

    #[test]
    fn add() {
        assert_eq!(Float::zero() + Float::from(0.765), Float::from(0.765));
        assert_eq!(
            Float::from(1.0 / 7.0) + Float::zero(),
            Float::from(1.0 / 7.0)
        );
        assert_eq!(Float::from(2.0) + Float::from(1.0), Float::from(3.0));
        assert_eq!(
            Float::from(1.0 / 3.0) + Float::from(1.0 / 3.0),
            Float::from(1.0 / 3.0 + 1.0 / 3.0)
        );
        assert_eq!(Float::from(1.125) + Float::from(3.875), Float::from(5.0));

        assert_eq!(Float::from(-1.) + Float::from(1.), Float::from(0.));
        assert_eq!(
            Float::from(12345.0) + Float::from(-2345.5),
            Float::from(9999.5)
        );
        assert_eq!(Float::from(1.25) + Float::from(-2.0), Float::from(-0.75));
        assert_eq!(Float::from(-6.75) + Float::from(-2.75), Float::from(-9.5));

        assert_eq!(Float::from(0.0) + Float::from(-1.0), Float::from(-1.0));
        assert_eq!(Float::from(-1.0) + Float::from(0.0), Float::from(-1.0));

        assert_eq!(
            Float::from(150.0) + Float::from(1.0 / 3.0),
            Float::from(150.0 + 1.0 / 3.0)
        );
    }

    #[test]
    fn sub() {
        assert_eq!(Float::from(1.) - Float::from(0.), Float::from(1.));
        assert_eq!(Float::from(1.25) - Float::from(2.125), Float::from(-0.875));
        assert_eq!(Float::from(2.25) - Float::from(-2.75), Float::from(5.0));
        assert_eq!(Float::from(-10.) - Float::from(-7.), Float::from(-3.));
        // A lot of precision-based tests from add() are not repeated here since the implementation
        // is just a wrapper. If the implementation ever changes, these tests should be duplicated.
    }

    #[test]
    fn neg() {
        assert_eq!(-Float::from(0.), Float::from(0.));
        assert_eq!(-Float::from(3.5), Float::from(-3.5));
        assert_eq!(
            -Float::from(2.7653).set_precision::<128>(),
            Float::from(-2.7653).set_precision::<128>()
        );
        assert_eq!(-Float::from(-1.0), Float::from(1.0));
        assert_eq!(--Float::from(-5.6), Float::from(-5.6));
    }

    #[test]
    fn mul() {
        assert_eq!(Float::from(0.) * Float::from(918.91), Float::from(0.));
        assert_eq!(Float::from(-758.5) * Float::from(0.), Float::from(0.));
        assert_eq!(
            Float::from(1.0) * Float::from(890123.12903),
            Float::from(890123.12903)
        );
        assert_eq!(Float::from(-2.5) * Float::from(5.0), Float::from(-12.5));
        assert_eq!(
            Float::from(-6.7) * Float::from(-2.1),
            Float::from(6.7 * 2.1)
        );

        assert_eq!(Float::from(1.0 / 4.0) * Float::from(4.0), Float::from(1.0));
        assert_eq!(Float::from(1.0 / 3.0) * Float::from(3.0), Float::from(1.0));
        assert_eq!(
            (Float::from(1.0 / 7.0) * Float::from(2.0)).set_precision::<128>(),
            Float::from(1.0 / 7.0).set_precision::<128>() * Float::from(2.0).set_precision::<128>()
        );
    }

    #[test]
    fn div() {
        assert_eq!(Float::from(0.) / Float::from(1.23), Float::from(0.));
        assert!(catch_unwind(|| Float::from(1.23) / Float::from(0.)).is_err());
        assert_eq!(Float::from(-33.25) / Float::from(0.5), Float::from(-66.5));
        assert_eq!(
            Float::from(-6.25) / Float::from(-12.0),
            Float::from(6.25 / 12.0)
        );

        assert_eq!(Float::from(1.0) / Float::from(4.0), Float::from(1.0 / 4.0));
        assert_eq!(Float::from(1.0) / Float::from(3.0), Float::from(1.0 / 3.0));
    }

    #[test]
    fn pow_u64() {
        assert_eq!(Float::from(2.0).pow_u64(0), Float::from(1.0));
        assert_eq!(Float::from(2.0).pow_u64(1), Float::from(2.0));
        assert!(catch_unwind(|| Float::from(0.0).pow_u64(0)).is_err());

        assert_eq!(Float::from(0.5).pow_u64(40), Float::from(0.5f64.powi(40)));
        assert!(!Float::from(0.3).pow_u64(10000).is_zero());
        // This is not the exact value (6.1917364224), but there doesn't seem to be a standard for
        // pow and this is close enough.
        assert_eq!(Float::from(1.2).pow_u64(10), Float::from(6.191736422399999));
        assert!(catch_unwind(|| Float::from(f64::MAX).pow_u64(u64::MAX)).is_err());
    }
}
