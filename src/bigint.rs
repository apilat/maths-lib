use std::cmp::{max, Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::convert::{From, TryFrom, TryInto};
use std::fmt::{self, Display, Formatter};
use std::ops::{Add, Div, Mul, Neg, Rem, Shl, Shr, Sub};

/// Unsigned integer with arbitrary precision.
///
/// # Operations
///
/// `Uint` and `&Uint` and `u64` versions are provided for each operation for user convenience.
/// `&Uint` implementations will clone if necessary, so it is preferable to pass in `Uint` if the
/// value is no longer needed.
///
/// # Representation
///
/// Internally, each number is represented as an integer in base `2**64` stored in a `Vec<u64>`.
/// The parts (as returned by `into_parts`) are arranged in least significant part order.
#[derive(Debug, Clone)]
pub struct Uint {
    pub(crate) data: Vec<u64>,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum ConversionError {
    AlphabetTooSmall,
    UnknownCharacter(char),
    ZeroBase,
    EmptyString,
}

impl Uint {
    /// Creates new Uint initialized to a value of zero.
    pub fn zero() -> Self {
        Self { data: Vec::new() }
    }

    /// Creates new Uint from the given data.
    /// Parts need to be arranged in least significant bit order.
    ///
    /// # Arguments
    ///
    /// * `data` - Parts in LSB order
    pub fn from_parts(data: Vec<u64>) -> Self {
        Self { data }
    }

    /// Creates new Uint from the given decimal string.
    ///
    /// # Arguments
    ///
    /// * `s` - Input string slice
    ///
    /// # Returns
    ///
    /// `ConversionError::UnknownCharacter` if the string contains a character other than `[0-9]`.
    /// `Uint` representing the given number otherwise.
    pub fn from_decimal(s: &str) -> Result<Self, ConversionError> {
        Uint::from_radix(s, 10, "0123456789")
    }

    /// Creates new Uint by parsing the input string in the given base.
    ///
    /// # Arguments
    ///
    /// * `s` - Input string slice
    /// * `base` - Base of input number. Must be `u64` due to the limitations of `Uint`.
    /// * `alphabet` - String slice containing characters in base. If the alphabet contains several
    /// of the same character, the left-most one will be used for conversion.
    ///
    /// # Returns
    ///
    /// `ConversionError::AlphabetTooSmall` if the alphabet doesn't have enough characters for the
    /// base.
    /// `ConversionError::UnknownCharacter` if the strings contains a character not in the alphabet.
    /// `ConversionError::EmptyString` if the input string is empty.
    /// `Uint` representing the given number if the input is correct.
    pub fn from_radix(s: &str, base: u64, alphabet: &str) -> Result<Self, ConversionError> {
        if alphabet.len() < base as usize {
            return Err(ConversionError::AlphabetTooSmall);
        }

        if s.is_empty() {
            return Err(ConversionError::EmptyString);
        }

        // TODO This could use a hashmap if the base is big enough to avoid O(N^2) cost of searching alphabet
        let mut n = Uint::zero();
        for c in s.chars() {
            // alphabet.find(c) does not work since it returns byte (not char) offset
            match alphabet.chars().position(|x| x == c) {
                None => return Err(ConversionError::UnknownCharacter(c)),
                Some(i) if i >= base as usize => return Err(ConversionError::UnknownCharacter(c)),
                Some(i) => n = (n * base) + i as u64,
            }
        }

        Ok(n)
    }

    /// Returns a String with the decimal representation of the number.
    pub fn to_decimal(&self) -> String {
        self.to_radix(10, "0123456789").unwrap()
    }

    /// Returns a String with the representation of the number in the given base and alphabet.
    ///
    /// # Arguments
    ///
    /// * `base` - Base of input number. Must be `u64` due to the limitations of `Uint`.
    /// * `alphabet` - String slice containing characters in base. If the alphabet does not have
    /// unique characters `Uint::from_radix(x.to_radix(..))` does not necessarily equal `x`.
    ///
    /// # Returns
    ///
    /// `ConversionError::AlphabetTooSmall` if the alphabet doesn't have enough characters for the
    /// base.
    /// `ConversionError::ZeroBase` if the base is zero.
    /// `String` representing this number otherwise.
    pub fn to_radix(&self, base: u64, alphabet: &str) -> Result<String, ConversionError> {
        if base == 0 {
            return Err(ConversionError::ZeroBase);
        }
        if alphabet.len() < base as usize {
            return Err(ConversionError::AlphabetTooSmall);
        }

        if self == &0 {
            return Ok(alphabet.chars().next().unwrap().to_string());
        }

        let mut cur = self.clone();

        let mut s = String::new();
        while cur.gt(&0) {
            let (next, digit) = cur.div_rem_u64(base);
            s.push(alphabet.chars().nth(digit as usize).unwrap());
            cur = next;
        }

        Ok(s.chars().rev().collect())
    }

    /// Returns the minimum number of bits needed to store this number.
    ///
    /// # Remarks
    ///
    /// This is defined as the number of significant bits of the number (the most significant one
    /// bit and all later bits).
    /// As a consequence of this definition, possibly unexpectedly, `bits(0) == 0`.
    pub fn bits(&self) -> usize {
        let mut bits = 64 * self.data.len();
        for v in self.data.iter().rev() {
            let leading_zeros = v.leading_zeros();
            bits -= leading_zeros as usize;
            if leading_zeros < 64 {
                break;
            }
        }
        bits
    }

    /// Returns the `n`th lowest bit of this number (0-indexed).
    ///
    /// # Panics
    ///
    /// This function *might* panic if `n >= bits()`. It might also return `false` instead, so you
    /// should not rely on this behaviour.
    pub fn bit(&self, n: usize) -> bool {
        assert!(n < self.data.len() * 64);
        self.data[n / 64] & (1 << (n % 64)) > 0
    }

    /// Sets the `n`th lowest bit of this number (0-indexed) to the given `value`.
    ///
    /// If the number does not have enough capacity to store this bit, the capacity will be
    /// increased (even if the bit is set to zero).
    pub fn set_bit(&mut self, n: usize, value: bool) {
        self.resize(n + 1);
        if value {
            self.data[n / 64] |= 1 << (n % 64);
        } else {
            self.data[n / 64] &= !(1 << (n % 64));
        }
    }

    /// Resize the number to be able to hold at least `b` bits. Any bits added will be set to zero.
    ///
    /// If the number of bits is lower than the current size, no action is performed.
    pub fn resize(&mut self, b: usize) {
        if b > self.bits() {
            self.data.resize((b + 63) / 64, 0);
        }
    }

    /// Takes this number to the power of `exponent`.
    /// Uint is not accepted as the exponent since exponents that big are not generally useful.
    ///
    /// # Panics
    ///
    /// Panics in the case of `0^0`.
    pub fn pow(self, mut exponent: u64) -> Self {
        assert!(!(self == 0 && exponent == 0));
        if self == 0 || self == 1 {
            return self;
        }

        // Binary exponentiation - O(N^2 log E)
        let mut cur = self;
        let mut value = Uint::from(1);
        while exponent > 0 {
            if exponent & 1 == 1 {
                value = value * &cur;
            }
            exponent >>= 1;
            cur = cur.clone() * cur;
        }
        value
    }

    /// Takes the number to the power of `exponent` modulo `divisor`.
    ///
    /// # Panics
    ///
    /// Panics when trying to calculate `0^0` and when `divisor == 0`.
    pub fn pow_mod(self, exponent: &Uint, divisor: &Uint) -> Self {
        assert!(!(self == 0 && exponent == &0));
        assert!(divisor != &0);

        let mut cur = self;
        let mut value = Uint::from(1);
        for i in 0..exponent.bits() {
            if exponent.bit(i) {
                value = (value * &cur) % divisor;
            }
            cur = (cur.clone() * cur) % divisor;
        }
        value
    }

    /// Divides this number by `divisor` and returns the `(quotient, remainder)` pair which
    /// satisfies `self = quotient * divisor + remainder` and `0 <= remainder < divisor`.
    ///
    /// This function automatically delegates to `div_rem_u64` if the divisor is small enough.
    ///
    /// # Panics
    ///
    /// Panics if `divisor = 0`.
    pub fn div_rem(self, divisor: &Uint) -> (Uint, Uint) {
        assert!(divisor != &0);
        match TryInto::<u64>::try_into(divisor) {
            Ok(n) => {
                let (q, r) = self.div_rem_u64(n);
                (q, Uint::from(r))
            }
            // Binary long division (https://en.wikipedia.org/wiki/Division_algorithm#Integer_division_(unsigned)_with_remainder)
            Err(()) => {
                let mut q = Uint::zero();
                let mut r = Uint::zero();
                for i in (0..self.bits()).rev() {
                    r = (r << 1) + self.bit(i) as u64;
                    if &r >= divisor {
                        r = r - divisor;
                        q.set_bit(i, true);
                    }
                }
                (q, r)
            }
        }
    }

    /// Equivalent to `div_rem` when the divisor is `64-bit` or less.
    pub fn div_rem_u64(mut self, divisor: u64) -> (Uint, u64) {
        assert!(divisor != 0);
        let mut remainder = 0;
        for i in (0..self.actual_len()).rev() {
            let (result, r) = Self::div_part(self.data[i], divisor, remainder);
            self.data[i] = result;
            remainder = r;
        }
        (self, remainder)
    }

    /// Returns the length of the underlying array which actually stores a number.
    /// Many operations only need to be done on this length.
    fn actual_len(&self) -> usize {
        (self.bits() + 63) / 64
    }

    /// Computes addition between parts of a number.
    /// `p1 + p2 + c_in = c_out << 64 + r`
    fn add_part(p1: u64, p2: u64, c: bool) -> (u64, bool) {
        let (r, c1) = p1.overflowing_add(p2);
        let (r, c2) = r.overflowing_add(c as u64);
        (r, c1 || c2)
    }

    /// Computes subtraction between parts of a number.
    /// `p1 - p2 - b_in = -b_out << 64 + r`
    fn sub_part(p1: u64, p2: u64, b: bool) -> (u64, bool) {
        let (r, b1) = p1.overflowing_sub(p2);
        let (r, b2) = r.overflowing_sub(b as u64);
        (r, b1 || b2)
    }

    /// Computes multiplication between parts of a number.
    /// `p1 * p2 + c_in = c_out << 64 + r`
    fn mul_part(p1: u64, p2: u64, c: u64) -> (u64, u64) {
        let r = p1 as u128 * p2 as u128 + c as u128;
        ((r & ((1 << 64) - 1)) as u64, (r >> 64) as u64)
    }

    /// Computes multiplication between parts of a number.
    /// `rem_in << 64 + p1 = p2 * r + rem_out`
    ///
    /// # Panics
    ///
    /// This function relies on the default Rust behaviour for division by zero, which is to panic.
    fn div_part(p1: u64, p2: u64, rem: u64) -> (u64, u64) {
        let r = p1 as u128 + ((rem as u128) << 64);
        let d = p2 as u128;
        ((r / d) as u64, (r % d) as u64)
    }
}

impl Default for Uint {
    fn default() -> Self {
        Uint::zero()
    }
}

impl Display for Uint {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.to_decimal())
    }
}

impl From<u64> for Uint {
    fn from(value: u64) -> Uint {
        Uint::from_parts(vec![value])
    }
}

impl TryFrom<Sint> for Uint {
    type Error = ();

    fn try_from(value: Sint) -> Result<Uint, ()> {
        if value.ge(&0) {
            Ok(value.abs)
        } else {
            Err(())
        }
    }
}

impl TryInto<u64> for &Uint {
    type Error = ();

    fn try_into(self) -> Result<u64, ()> {
        let bits = self.bits();
        if bits == 0 {
            Ok(0)
        } else if bits <= 64 {
            Ok(self.data[0])
        } else {
            Err(())
        }
    }
}

impl TryInto<u64> for Uint {
    type Error = ();
    fn try_into(self) -> Result<u64, ()> {
        (&self).try_into()
    }
}

impl PartialEq for Uint {
    fn eq(&self, other: &Self) -> bool {
        self.actual_len() == other.actual_len()
            && (0..self.actual_len()).all(|i| self.data[i] == other.data[i])
    }
}

impl PartialEq<u64> for Uint {
    fn eq(&self, other: &u64) -> bool {
        self.bits() <= 64 && other == self.data.get(0).unwrap_or(&0)
    }
}

impl Eq for Uint {}

impl PartialOrd for Uint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialOrd<u64> for Uint {
    fn partial_cmp(&self, other: &u64) -> Option<Ordering> {
        match TryInto::<u64>::try_into(self) {
            Ok(x) => x.partial_cmp(other),
            Err(()) => Some(Ordering::Greater),
        }
    }
}

impl Ord for Uint {
    fn cmp(&self, other: &Self) -> Ordering {
        self.actual_len().cmp(&other.actual_len()).then_with(|| {
            for i in (0..self.actual_len()).rev() {
                match self.data[i].cmp(&other.data[i]) {
                    Ordering::Equal => (),
                    o => return o,
                }
            }
            Ordering::Equal
        })
    }
}

impl Add<&Uint> for Uint {
    type Output = Uint;

    fn add(mut self, other: &Uint) -> Uint {
        self.resize(max(self.bits() + 1, other.bits() + 1));
        assert!(self.data.len() >= other.actual_len());

        let mut carry = false;
        for i in 0..self.data.len() {
            let (result, c) = if i < other.actual_len() {
                Self::add_part(self.data[i], other.data[i], carry)
            } else {
                if !carry {
                    break;
                }
                Self::add_part(self.data[i], 0, carry)
            };
            self.data[i] = result;
            carry = c;
        }

        assert!(!carry);

        self
    }
}

impl Add<Uint> for Uint {
    type Output = Uint;

    fn add(self, other: Uint) -> Uint {
        self + &other
    }
}

impl Add<u64> for Uint {
    type Output = Uint;

    fn add(mut self, other: u64) -> Uint {
        self.resize(max(self.bits(), (64 - other.leading_zeros()) as usize) + 1);

        let mut carry = false;
        for i in 0..self.data.len() {
            let (result, c) = if i == 0 {
                Self::add_part(self.data[i], other, carry)
            } else {
                if !carry {
                    break;
                }
                Self::add_part(self.data[i], 0, carry)
            };
            self.data[i] = result;
            carry = c;
        }

        assert!(!carry);

        self
    }
}

impl Sub<&Uint> for Uint {
    type Output = Uint;

    fn sub(mut self, other: &Uint) -> Uint {
        assert!(self.bits() >= other.bits());

        let mut borrow = false;
        for i in 0..self.data.len() {
            let (result, b) = if i < other.actual_len() {
                Self::sub_part(self.data[i], other.data[i], borrow)
            } else {
                if !borrow {
                    break;
                }
                Self::sub_part(self.data[i], 0, borrow)
            };
            self.data[i] = result;
            borrow = b;
        }

        assert!(!borrow);

        self
    }
}

impl Sub<Uint> for Uint {
    type Output = Uint;

    fn sub(self, other: Uint) -> Uint {
        self - &other
    }
}

impl Sub<u64> for Uint {
    type Output = Uint;

    fn sub(mut self, other: u64) -> Uint {
        assert!(self.bits() >= (64 - other.leading_zeros()) as usize);

        let mut borrow = false;
        for i in 0..self.data.len() {
            let (result, b) = if i == 0 {
                Self::sub_part(self.data[i], other, borrow)
            } else {
                if !borrow {
                    break;
                }
                Self::sub_part(self.data[i], 0, borrow)
            };
            self.data[i] = result;
            borrow = b;
        }

        assert!(!borrow);

        self
    }
}

impl Mul<u64> for Uint {
    type Output = Uint;

    fn mul(mut self, other: u64) -> Uint {
        self.resize(self.bits() + (64 - other.leading_zeros()) as usize);

        let mut carry = 0;
        for i in 0..self.data.len() {
            let (result, c) = Self::mul_part(self.data[i], other, carry);
            self.data[i] = result;
            carry = c;
        }

        assert!(carry == 0);

        self
    }
}

impl Mul<&Uint> for Uint {
    type Output = Uint;

    fn mul(self, other: &Uint) -> Uint {
        self * other.clone()
    }
}

impl Mul<Uint> for Uint {
    type Output = Uint;

    // Clippy doesn't deal well with complex arithmetic implementations (https://github.com/rust-lang/rust-clippy/issues/3215)
    #[allow(clippy::suspicious_arithmetic_impl)]
    fn mul(mut self, mut other: Uint) -> Uint {
        if let Ok(n) = TryInto::<u64>::try_into(&other) {
            return self * n;
        }

        // Karatsuba's algorithm (https://en.wikipedia.org/wiki/Karatsuba_algorithm)
        let m = other.actual_len() / 2;
        let x1 = if m <= self.data.len() {
            Uint::from_parts(self.data.split_off(m))
        } else {
            Uint::zero()
        };
        let x0 = Uint::from_parts(self.data);
        let y1 = Uint::from_parts(other.data.split_off(m));
        let y0 = Uint::from_parts(other.data);

        let mut z1 = (x0.clone() + &x1) * (y0.clone() + &y1);
        let z0 = x0 * y0;
        let z2 = x1 * y1;
        z1 = z1 - &z2 - &z0;

        (z2 << (2 * m * 64)) + (z1 << (m * 64)) + z0
    }
}

impl Div<u64> for Uint {
    type Output = Uint;

    fn div(self, other: u64) -> Uint {
        self.div_rem_u64(other).0
    }
}

impl Div<&Uint> for Uint {
    type Output = Uint;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, d: &Uint) -> Uint {
        self.div_rem(d).0
    }
}

impl Div<Uint> for Uint {
    type Output = Uint;

    fn div(self, other: Uint) -> Uint {
        self / &other
    }
}

impl Rem<u64> for &Uint {
    type Output = u64;

    fn rem(self, other: u64) -> u64 {
        assert!(other != 0);
        let mut remainder = 0;
        for i in (0..self.actual_len()).rev() {
            let (_, r) = Uint::div_part(self.data[i], other, remainder);
            remainder = r;
        }
        remainder
    }
}

impl Rem<u64> for Uint {
    type Output = u64;

    fn rem(self, other: u64) -> u64 {
        &self % other
    }
}

impl Rem<&Uint> for Uint {
    type Output = Uint;

    fn rem(self, d: &Uint) -> Uint {
        self.div_rem(d).1
    }
}

impl Rem<Uint> for Uint {
    type Output = Uint;

    fn rem(self, other: Uint) -> Uint {
        self % &other
    }
}

impl Shl<usize> for Uint {
    type Output = Uint;

    fn shl(mut self, mut shift: usize) -> Uint {
        if shift >= 64 {
            let extend = shift / 64;
            shift %= 64;

            self.data.resize(self.data.len() + extend, 0);
            self.data.rotate_right(extend);
        }

        if shift > 0 {
            self.resize(self.bits() + shift);

            let mut carry = 0;
            for i in 0..self.data.len() {
                let mask: u64 = !((1 << (64 - shift)) - 1);
                let c = (self.data[i] & mask) >> (64 - shift);
                self.data[i] = self.data[i] << shift | carry;
                carry = c;
            }

            assert!(carry == 0);
        }

        self
    }
}

impl Shr<usize> for Uint {
    type Output = Uint;

    fn shr(mut self, mut shift: usize) -> Uint {
        if shift >= self.bits() {
            // Prevents panic on rotate_left(shrink)
            return Uint::from(0);
        }

        if shift >= 64 {
            let shrink = shift / 64;
            shift %= 64;

            self.data.rotate_left(shrink);
            self.data.truncate(self.data.len() - shrink);
        }

        if shift > 0 {
            let mut carry = 0;
            for i in (0..self.data.len()).rev() {
                let mask: u64 = (1 << shift) - 1;
                let c = (self.data[i] & mask) << (64 - shift);
                self.data[i] = carry | self.data[i] >> shift;
                carry = c;
            }
        }

        self
    }
}

#[cfg(test)]
mod uint_tests {
    use crate::bigint::{ConversionError, Uint};
    use std::convert::TryInto;
    use std::panic::catch_unwind;

    #[test]
    fn bits() {
        assert_eq!(Uint::zero().bits(), 0);
        assert_eq!(Uint::from_parts(vec![0]).bits(), 0);
        assert_eq!(Uint::from_parts(vec![1]).bits(), 1);
        assert_eq!(Uint::from_parts(vec![0x40]).bits(), 7);
        assert_eq!(Uint::from_parts(vec![0x4020]).bits(), 15);

        assert_eq!(Uint::from_parts(vec![3, 0]).bits(), 2);
        assert_eq!(Uint::from_parts(vec![3, 0, 0, 0]).bits(), 2);

        assert_eq!(Uint::from_parts(vec![0, 2]).bits(), 66);
        assert_eq!(Uint::from_parts(vec![0x1234567, 2]).bits(), 66);
        assert_eq!(
            Uint::from_parts(vec![
                0x3141592653589793,
                0x2384626433832795,
                0x0288419716939937,
                0x1
            ])
            .bits(),
            193
        );
        assert_eq!(Uint::from_parts(vec![4, 3, 2, 1, 0, u64::MAX]).bits(), 384);
    }

    #[test]
    fn bit() {
        let n = Uint::from_parts(vec![0x890438902748932, 0x1020]);
        assert_eq!(n.bit(0), false);
        assert_eq!(n.bit(1), true);
        assert_eq!(n.bit(5), true);
        assert_eq!(n.bit(63), false);
        assert_eq!(n.bit(64), false);
        assert_eq!(n.bit(69), true);

        let x = Uint::from_parts(vec![u64::MAX]);
        assert_eq!(x.bit(1), true);
        assert_eq!(x.bit(63), true);
        assert!(catch_unwind(|| x.bit(64)).is_err());
    }

    #[test]
    fn set_bit() {
        let mut n = Uint::from_parts(vec![0x1234, 0x5678]);
        n.set_bit(0, true);
        n.set_bit(2, false);
        n.set_bit(64, false);
        assert_eq!(n, Uint::from_parts(vec![0x1231, 0x5678]));
        n.set_bit(65, true);
        n.set_bit(7, true);
        assert_eq!(n, Uint::from_parts(vec![0x12b1, 0x567a]));
        n.set_bit(127, true);
        n.set_bit(128, true);
        assert_eq!(n, Uint::from_parts(vec![0x12b1, 0x800000000000567a, 0x1]));
    }

    #[test]
    fn eq() {
        let zero = Uint::zero();
        let extended_zero = Uint::from_parts(vec![0, 0]);
        let one = Uint::from_parts(vec![1]);
        let extended_one = Uint::from_parts(vec![1, 0]);

        let high = Uint::from_parts(vec![0, 1, 0]);

        assert_eq!(zero, Uint::zero());
        assert_eq!(zero, extended_zero);
        assert_eq!(one, extended_one);
        assert_ne!(zero, one);
        assert_ne!(zero, extended_one);
        assert_ne!(one, extended_zero);

        assert_ne!(zero, high);
        assert_ne!(one, high);
    }

    #[test]
    fn eq_u64() {
        assert_eq!(Uint::zero(), 0);
        assert_eq!(Uint::from_parts(vec![10, 0]), 10);
        assert_eq!(Uint::from_parts(vec![849230]), 849230);
        assert_ne!(Uint::from_parts(vec![172]), 171);

        assert_eq!(Uint::from_parts(vec![u64::MAX]), u64::MAX);
        assert_ne!(Uint::from_parts(vec![u64::MAX, 1]), u64::MAX);

        assert_ne!(Uint::from_parts(vec![3, 2]), 3);
        assert_ne!(Uint::from_parts(vec![3, 2, 0]), 2);
    }

    #[test]
    fn cmp() {
        use std::cmp::{Ord, Ordering};

        let zero = Uint::zero();
        let extended_zero = Uint::from_parts(vec![0, 0]);
        let one = Uint::from_parts(vec![1]);
        let extended_one = Uint::from_parts(vec![1, 0]);

        let high = Uint::from_parts(vec![0, 1]);

        assert_eq!(zero.cmp(&one), Ordering::Less);
        assert_eq!(one.cmp(&zero), Ordering::Greater);
        assert_eq!(zero.cmp(&extended_zero), Ordering::Equal);
        assert_eq!(one.cmp(&extended_one), Ordering::Equal);

        assert_eq!(high.cmp(&zero), Ordering::Greater);
        assert_eq!(high.cmp(&extended_one), Ordering::Greater);
    }

    #[test]
    fn cmp_u64() {
        use std::cmp::{Ordering, PartialOrd};

        let zero = Uint::zero();
        let one = Uint::from(1);
        let max = Uint::from_parts(vec![u64::MAX, 0, 0]);
        let over = Uint::from_parts(vec![0, 1]);

        assert_eq!(zero.partial_cmp(&0), Some(Ordering::Equal));
        assert_eq!(zero.partial_cmp(&1), Some(Ordering::Less));
        assert_eq!(zero.partial_cmp(&u64::MAX), Some(Ordering::Less));

        assert_eq!(one.partial_cmp(&0), Some(Ordering::Greater));
        assert_eq!(one.partial_cmp(&1), Some(Ordering::Equal));
        assert_eq!(one.partial_cmp(&u64::MAX), Some(Ordering::Less));

        assert_eq!(max.partial_cmp(&0), Some(Ordering::Greater));
        assert_eq!(max.partial_cmp(&u64::MAX), Some(Ordering::Equal));

        assert_eq!(over.partial_cmp(&0), Some(Ordering::Greater));
        assert_eq!(over.partial_cmp(&u64::MAX), Some(Ordering::Greater));
    }

    #[test]
    fn add() {
        assert_eq!(Uint::zero() + Uint::zero(), Uint::from_parts(vec![0]));
        assert_eq!(
            Uint::zero() + Uint::from_parts(vec![890]),
            Uint::from_parts(vec![890])
        );
        assert_eq!(
            Uint::from_parts(vec![123456]) + Uint::from_parts(vec![876543]),
            Uint::from_parts(vec![999999])
        );

        assert_eq!(
            Uint::from_parts(vec![0, 1]) + Uint::from_parts(vec![3, 0]),
            Uint::from_parts(vec![3, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![0, 1, 0]) + Uint::from_parts(vec![3, 0]),
            Uint::from_parts(vec![3, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![0, 1, 0]) + Uint::from_parts(vec![3, 0, 0]),
            Uint::from_parts(vec![3, 1])
        );

        assert_eq!(
            Uint::from_parts(vec![u64::MAX]) + Uint::from_parts(vec![1]),
            Uint::from_parts(vec![0, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX, 0, 0]) + Uint::from_parts(vec![1]),
            Uint::from_parts(vec![0, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![3]) + Uint::from_parts(vec![u64::MAX]),
            Uint::from_parts(vec![2, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX, 530]) + Uint::from_parts(vec![1]),
            Uint::from_parts(vec![0, 531])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX, u64::MAX, u64::MAX]) + Uint::from_parts(vec![1]),
            Uint::from_parts(vec![0, 0, 0, 1])
        );

        assert_eq!(
            Uint::from_parts(vec![125]) + 8641,
            Uint::from_parts(vec![8766])
        );
        assert_eq!(
            Uint::from_parts(vec![1, 2, 3, 0]) + 629,
            Uint::from_parts(vec![630, 2, 3])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX - 3]) + 10,
            Uint::from_parts(vec![6, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX, u64::MAX, 2]) + 187,
            Uint::from_parts(vec![186, 0, 3])
        );
    }

    #[test]
    fn sub() {
        assert_eq!(Uint::zero() - Uint::zero(), Uint::zero());
        assert_eq!(
            Uint::from_parts(vec![927]) - Uint::zero(),
            Uint::from_parts(vec![927])
        );
        assert_eq!(
            Uint::from_parts(vec![983027]) - Uint::from_parts(vec![84903]),
            Uint::from_parts(vec![898124])
        );

        assert_eq!(
            Uint::from_parts(vec![2, 3]) - Uint::from_parts(vec![2, 2]),
            Uint::from_parts(vec![0, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![2, 3, 0, 0]) - Uint::from_parts(vec![2, 2]),
            Uint::from_parts(vec![0, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![2, 3]) - Uint::from_parts(vec![2, 2, 0, 0]),
            Uint::from_parts(vec![0, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![7, 8, 2]) - Uint::from_parts(vec![7, 8, 2]),
            Uint::from_parts(vec![0])
        );

        assert_eq!(
            Uint::from_parts(vec![0, 1]) - Uint::from_parts(vec![1]),
            Uint::from_parts(vec![u64::MAX])
        );
        assert_eq!(
            Uint::from_parts(vec![0, 1, 0, 0]) - Uint::from_parts(vec![1]),
            Uint::from_parts(vec![u64::MAX])
        );
        assert_eq!(
            Uint::from_parts(vec![0, 1]) - Uint::from_parts(vec![1, 0, 0]),
            Uint::from_parts(vec![u64::MAX])
        );
        assert_eq!(
            Uint::from_parts(vec![0, 0, 2]) - Uint::from_parts(vec![2, 1]),
            Uint::from_parts(vec![u64::MAX - 1, u64::MAX - 1, 1])
        );

        assert_eq!(
            Uint::from_parts(vec![1920]) - 917,
            Uint::from_parts(vec![1003])
        );
        assert_eq!(
            Uint::from_parts(vec![19, 719, 58, 17]) - 7,
            Uint::from_parts(vec![12, 719, 58, 17])
        );
        assert_eq!(
            Uint::from_parts(vec![10, 1]) - 15,
            Uint::from_parts(vec![u64::MAX - 4, 0])
        );
        assert_eq!(
            Uint::from_parts(vec![0, 0, 0, 1]) - 7,
            Uint::from_parts(vec![u64::MAX - 6, u64::MAX, u64::MAX])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX]) - u64::MAX,
            Uint::from_parts(vec![0])
        );

        assert!(catch_unwind(|| { Uint::from_parts(vec![1]) - Uint::zero() }).is_ok());
        assert!(catch_unwind(|| { Uint::zero() - Uint::from_parts(vec![1]) }).is_err());
        assert!(
            catch_unwind(|| { Uint::from_parts(vec![10]) - Uint::from_parts(vec![15]) }).is_err()
        );
        assert!(
            catch_unwind(|| { Uint::from_parts(vec![0, 1]) - Uint::from_parts(vec![1, 1]) })
                .is_err()
        );
        assert!(catch_unwind(|| {
            Uint::from_parts(vec![u64::MAX, 1, 0]) - Uint::from_parts(vec![0, 2])
        })
        .is_err());

        assert!(catch_unwind(|| { Uint::from_parts(vec![1]) - 2 }).is_err());
        assert!(
            catch_unwind(|| { Uint::from_parts(vec![0x87ef6297dc9619a7]) - u64::MAX }).is_err()
        );
    }

    #[test]
    fn mul_u64() {
        assert_eq!(
            Uint::from_parts(vec![19, 173, 18, 0]) * 0,
            Uint::from_parts(vec![0])
        );
        assert_eq!(
            Uint::from_parts(vec![9178, 123]) * 1,
            Uint::from_parts(vec![9178, 123])
        );
        assert_eq!(
            Uint::from_parts(vec![9032, 3892, 1028]) * 982712,
            Uint::from_parts(vec![8875854784, 3824715104, 1010227936])
        );

        assert_eq!(
            Uint::from_parts(vec![u64::MAX, u64::MAX]) * 3,
            Uint::from_parts(vec![u64::MAX - 2, u64::MAX, 2])
        );
        assert_eq!(
            Uint::from_parts(vec![0, 81]) * u64::MAX,
            Uint::from_parts(vec![0, u64::MAX - 80, 80])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX]) * u64::MAX,
            Uint::from_parts(vec![1, u64::MAX - 1, 0])
        );
    }

    #[test]
    fn mul() {
        assert_eq!(
            Uint::from_parts(vec![20, 45]) * Uint::from_parts(vec![0, 0, 0]),
            Uint::zero()
        );
        assert_eq!(
            Uint::zero() * Uint::from_parts(vec![1923, 9012, 0]),
            Uint::zero()
        );
        assert_eq!(
            Uint::from_parts(vec![192, 18, 0]) * Uint::from_parts(vec![0, 2]),
            Uint::from_parts(vec![0, 384, 36])
        );

        assert_eq!(
            Uint::from_parts(vec![u64::MAX]) * Uint::from_parts(vec![1, 2]),
            Uint::from_parts(vec![u64::MAX, u64::MAX - 1, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![0, u64::MAX]) * Uint::from_parts(vec![0, 0, u64::MAX, 0]),
            Uint::from_parts(vec![0, 0, 0, 1, u64::MAX - 1])
        );
        assert_eq!(
            Uint::from_parts(vec![16342852735464590591, 15614871612210292738])
                * Uint::from_parts(vec![15152178578056940779, 16769806615179159503]),
            Uint::from_parts(vec![
                6606500799371911701,
                11929606420057768754,
                11419772939158877255,
                14195371075311881468
            ])
        );
    }

    #[test]
    fn div_u64() {
        assert_eq!(
            Uint::from_parts(vec![198, 92, 832]) / 2,
            Uint::from_parts(vec![99, 46, 416])
        );
        assert_eq!(
            Uint::from_parts(vec![15, 4]) / 3,
            Uint::from_parts(vec![6148914691236517210, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![38, 918, 69]) / 817292,
            Uint::from_parts(vec![12063923070224115640, 1557369142345647, 0])
        );
        assert_eq!(
            Uint::from_parts(vec![2, 1, 0, 0, 0]) / 65,
            Uint::from_parts(vec![283796062672454640])
        );

        assert_eq!(
            Uint::from_parts(vec![0, 2]) / u64::MAX,
            Uint::from_parts(vec![2, 0])
        );
        assert_eq!(
            Uint::from_parts(vec![u64::MAX, u64::MAX]) / u64::MAX,
            Uint::from_parts(vec![1, 1])
        );
        assert_eq!(
            Uint::from_parts(vec![10, u64::MAX, u64::MAX - 1]) / (u64::MAX - 2),
            Uint::from_parts(vec![5, 2, 1])
        );

        assert!(catch_unwind(|| { Uint::zero() / 0 }).is_err());
        assert!(catch_unwind(|| { Uint::from_parts(vec![1]) / 0 }).is_err());

        assert_eq!(
            Uint::from_parts(vec![
                9533802695540170597,
                12574694895526126533,
                13867714751385274989
            ])
            .div_rem_u64(13492782463727130648),
            (
                Uint::from_parts(vec![13699865310595021680, 512591081490670165, 1]),
                362102133731577061
            )
        );
    }

    #[test]
    fn div() {
        assert!(catch_unwind(|| { Uint::zero() / 0 }).is_err());
        assert_eq!(
            Uint::from_parts(vec![793, 91, 3]) / Uint::from(1),
            Uint::from_parts(vec![793, 91, 3])
        );
        assert_eq!(
            Uint::from_parts(vec![91559, 3902, 9182]) / Uint::from(492),
            Uint::from_parts(vec![9823266153072972794, 12222842617945759818, 18])
        );
        assert_eq!(
            Uint::from_parts(vec![19384, 389403892, 129, 0, 0]) / Uint::from_parts(vec![1092, 489]),
            Uint::from_parts(vec![4866318988770800740])
        );

        assert_eq!(
            Uint::from_parts(vec![0, 2, 2]) / Uint::from_parts(vec![u64::MAX, u64::MAX]),
            Uint::from(2)
        );
        assert_eq!(
            Uint::from_parts(vec![0, u64::MAX]) / Uint::from_parts(vec![0, 0, 1]),
            Uint::from(0)
        );
        assert_eq!(
            Uint::from_parts(vec![0, 0, 1, 0]) / Uint::from_parts(vec![u64::MAX, 2]),
            Uint::from_parts(vec![6148914691236517205])
        );

        assert_eq!(
            Uint::from_parts(vec![
                10561435241148333507,
                15261790532601508953,
                14985749380858613766,
                14293400543786544794,
                9475855164728546008
            ])
            .div_rem(&Uint::from_parts(vec![
                17454839348298657646,
                17133209053321347417,
                13388894075822383118
            ])),
            (
                Uint::from_parts(vec![14183709698052300918, 13055497647033982693]),
                Uint::from_parts(vec![
                    8713864209567293711,
                    8421653420713699583,
                    10185767148135475928
                ])
            )
        );
    }

    #[test]
    fn rem_u64() {
        assert_eq!(Uint::from_parts(vec![193]) % 3, 1);
        assert_eq!(Uint::from_parts(vec![7839, 0, 0]) % 849032, 7839);
        assert_eq!(
            Uint::from_parts(vec![190, 948]) % 1927384283110,
            1500037091038
        );

        assert_eq!(
            Uint::from_parts(vec![u64::MAX, u64::MAX - 1, 0, 0]) % u64::MAX,
            u64::MAX - 1
        );
        assert_eq!(Uint::from_parts(vec![0, 0, u64::MAX]) % 23, 19);

        assert!(catch_unwind(|| { Uint::zero() % 0 }).is_err());
        assert!(catch_unwind(|| { Uint::from_parts(vec![1, 0, 0]) % 0 }).is_err());
    }

    #[test]
    fn rem() {
        assert_eq!(
            Uint::from_parts(vec![0, 0, 1]) % Uint::from(65537),
            Uint::from(1)
        );
        assert!(catch_unwind(|| Uint::from(123) % Uint::zero()).is_err());
        assert_eq!(
            Uint::from_parts(vec![123, 456]) % Uint::from_parts(vec![0, 0, 1]),
            Uint::from_parts(vec![123, 456])
        );

        assert_eq!(
            Uint::from_parts(vec![123890, 83920, 7894]) % Uint::from_parts(vec![9049, 81923]),
            Uint::from_parts(vec![911945528099968663, 71919])
        );
        assert_eq!(
            Uint::from_parts(vec![4, 3, 2, u64::MAX]) % Uint::from_parts(vec![0, 0, u64::MAX - 1]),
            Uint::from_parts(vec![4, 3, 4])
        );
    }

    #[test]
    fn pow() {
        assert_eq!(Uint::from_parts(vec![0]).pow(9430280), Uint::from(0));
        assert_eq!(Uint::from_parts(vec![1]).pow(847281), Uint::from(1));
        assert!(catch_unwind(|| Uint::from_parts(vec![0]).pow(0)).is_err());
        assert_eq!(Uint::from_parts(vec![3]).pow(10), Uint::from(59049));
        assert_eq!(Uint::from(4).pow(0), Uint::from(1));

        assert_eq!(Uint::from_parts(vec![132]).pow(924), Uint::from_decimal("2572245607108492185367065655964763378977199944692917434519695054122193477007597148029484397240431296368848398513612418919396798387307507828640868119408821858385111861430978001705970398832404796104582633958847754056881768346589384525082490228005957787584810509320330654764639633595595346403908274722974544427522484359628236501703912443676375359714583432587388052748409410879560268016734099643506775476566740705395699724433494955327848021157031655475445866802559092790866641818659800318643640130519080691759065398742832253356879125883661449003871288152869220352624312268043578038266277961344186258420690256224559493608048383597795686412184264719054837179530869436498017425918940083230006962009676792662514825703234042143049217399319040205041383597172091201300651112749211920666764483532273315108177976897038030266908698581633053675898168740922865782359543604250663377216280979693983522004791461196124518745001924861517553505361361610628126585219250563248079323326593906028556714097682689879551262098005252808110613042146079758181794725498856009903859911332292187221031938432114964169133718589375746865967906214755516759689915430991091083200108174405173075415226653312560602048020235074991548914763982091886014647336458106654010634310233420838237337502717383532182679186269072486382297893932585581937367644382649397170427291453655787868646905210194856499199085784551166646965309471397729446378719634306471819230545920655997218788476439890142315968537379427502824163936550509790708157992511603357073398348091830564484505715181531674294576467024728931941879396561988165012242002909318067941633114531193690174421633400035133737911246316688185575741298985353740580062902979371364971634974091687093229901736348849668644077787519013307354435489823519902724334874813476259543945476880140052009493324468768743357002087881576604989476940810697653305859568291518648277769404691561033782627348122795099478836781517325942689425082281627559749059307016141682305421057537867776").unwrap());
        assert_eq!(Uint::from_parts(vec![2, 2, 0]).pow(15), Uint::from_decimal("319334449525555170124686962029696352556430411129901139618070016582859231389918376305750745411681082936169067166925679785464040858619196257811388029132009743977441290222278965096323013302928099696016039835851688642858773021975321095658838769505757829359666669435971584015952864052057111324033024").unwrap());
    }

    #[test]
    fn pow_mod() {
        assert_eq!(
            Uint::from(3).pow_mod(&Uint::from(200), &Uint::from(256)),
            Uint::from(161)
        );
        assert_eq!(
            Uint::from(1).pow_mod(&Uint::from(1000), &Uint::from_parts(vec![0, 1])),
            Uint::from(1)
        );
        assert!(catch_unwind(|| Uint::from(0).pow_mod(&Uint::from(10), &Uint::from(0))).is_err());
        assert!(catch_unwind(|| Uint::from(0).pow_mod(&Uint::from(0), &Uint::from(100))).is_err());
        assert_eq!(
            Uint::from_parts(vec![2, 3, 0])
                .pow_mod(&Uint::from_parts(vec![3, 1]), &Uint::from_parts(vec![1, 1])),
            Uint::from_parts(vec![0, 1])
        );

        assert_eq!(
            Uint::from_parts(vec![894, 1092, 192]).pow_mod(
                &Uint::from_parts(vec![4, 10]),
                &Uint::from_parts(vec![10, 43])
            ),
            Uint::from_decimal("558530821095680391088").unwrap()
        );
    }

    #[test]
    fn shl() {
        assert_eq!(Uint::zero() << 10, Uint::zero());
        assert_eq!(Uint::from(1) << 70, Uint::from_parts(vec![0, 64]));
        assert_eq!(
            Uint::from_parts(vec![873, 19346]) << 190,
            Uint::from_parts(vec![0, 0, 4611686018427387904, 9223372036854776026, 4836])
        );
        assert_eq!(
            Uint::from_parts(vec![11535312845897204128, 11997976677247134569]) << 32,
            Uint::from_parts(vec![12156343080886206464, 5673598100725672426, 2793496632])
        );
        assert_eq!(
            Uint::from_parts(vec![3]) << (64 * 7),
            Uint::from_parts(vec![0, 0, 0, 0, 0, 0, 0, 3])
        );
        assert_eq!(
            Uint::from_parts(vec![13, 0, 0]) << 15,
            Uint::from_parts(vec![425984])
        );
    }

    #[test]
    fn shr() {
        assert_eq!(Uint::zero() >> 10, Uint::zero());
        assert_eq!(Uint::from(4096) >> 10, Uint::from(4));
        assert_eq!(
            Uint::from_parts(vec![5653224984818394805, 8490324]) >> 50,
            Uint::from_parts(vec![139105473437])
        );
        assert_eq!(
            Uint::from_parts(vec![849302432, 1902378907483, 18290389021, 189209032]) >> 125,
            Uint::from_parts(vec![146323112168, 1513672256])
        );
        assert_eq!(
            Uint::from_parts(vec![4096, 1, 0, 0]) >> 10,
            Uint::from_parts(vec![18014398509481988])
        );
        assert_eq!(Uint::from(1) >> 300, Uint::from(0));
    }

    #[test]
    fn from_decimal() {
        assert_eq!(Uint::from_decimal(""), Err(ConversionError::EmptyString));
        assert_eq!(
            Uint::from_decimal("88341"),
            Ok(Uint::from_parts(vec![88341]))
        );
        assert_eq!(
            Uint::from_decimal("18446744073709551617"),
            Ok(Uint::from_parts(vec![1, 1]))
        );
        assert_eq!(
            Uint::from_decimal("74893271894037219483729810473291874982174398"),
            Ok(Uint::from_parts(vec![
                11103646884238728894,
                10054676327215032013,
                220091
            ]))
        );

        assert_eq!(
            Uint::from_decimal("-1"),
            Err(ConversionError::UnknownCharacter('-'))
        );
        assert_eq!(
            Uint::from_decimal("9349e12"),
            Err(ConversionError::UnknownCharacter('e'))
        );
    }

    #[test]
    fn from_radix() {
        assert_eq!(
            Uint::from_radix("", 10, "abcdefghijk"),
            Err(ConversionError::EmptyString)
        );
        assert_eq!(
            Uint::from_radix("1000000000000", 16, "0123456789abcdef"),
            Ok(Uint::from_parts(vec![281474976710656]))
        );
        assert_eq!(
            Uint::from_radix("1000000000000", 2, "0123456789abcdef"),
            Ok(Uint::from_parts(vec![4096]))
        );
        assert_eq!(
            Uint::from_radix(
                "9726ed81b8362d28f00e75f4991ca513bca1b84127f906bad2309985549ad3de",
                16,
                "0123456789abcdef"
            ),
            Ok(Uint::from_parts(vec![
                15145774344776111070,
                13592347740363884218,
                17297892912177915155,
                10891653890239245608
            ]))
        );

        assert_eq!(
            Uint::from_radix("1234", 6, "051423"),
            Ok(Uint::from_parts(vec![609]))
        );
        assert_eq!(
            Uint::from_radix("😉🚀😂😂", 5, "😂😎🚀☹😉"),
            Ok(Uint::from_parts(vec![550]))
        );

        assert_eq!(
            Uint::from_radix("BBBB", 4, "ABCB"),
            Ok(Uint::from_parts(vec![85]))
        );

        assert_eq!(
            Uint::from_radix("1", 10, "01234567"),
            Err(ConversionError::AlphabetTooSmall)
        );
        assert_eq!(
            Uint::from_radix("0x12", 16, "0123456789abcdef"),
            Err(ConversionError::UnknownCharacter('x'))
        );
    }

    #[test]
    fn to_decimal() {
        assert_eq!(Uint::zero().to_decimal(), String::from("0"));
        assert_eq!(
            Uint::from_parts(vec![102873]).to_decimal(),
            String::from("102873")
        );
        assert_eq!(
            Uint::from_parts(vec![0, 3, 1]).to_decimal(),
            String::from("340282366920938463518714839652896866304")
        );
        assert_eq!(
            Uint::from_parts(vec![8490328094382094, 91748, 0, 0]).to_decimal(),
            String::from("1692451883765032036046862")
        );

        assert_eq!(
            Uint::from_decimal("26587412769138547895213178972233001575")
                .unwrap()
                .to_decimal(),
            String::from("26587412769138547895213178972233001575")
        );
    }

    #[test]
    fn to_radix() {
        assert_eq!(Uint::zero().to_radix(5, "ABCDE"), Ok(String::from("A")));
        assert_eq!(
            Uint::from_parts(vec![0x123456789abcdef, 0x314159]).to_radix(16, "0123456789abcdef"),
            Ok(String::from("3141590123456789abcdef"))
        );
        assert_eq!(Uint::from_parts(vec![0x123456789abcdef, 0x314159]).to_radix(2, "0123456789abcdef"), Ok(String::from("11000101000001010110010000000100100011010001010110011110001001101010111100110111101111")));

        assert_eq!(
            Uint::from_parts(vec![0, 1, 0]).to_radix(3, "XYZ"),
            Ok(String::from("YYYYZZZXXZZYZZYZXYXYZYYXZXYZXZYXZYXZYYZZY"))
        );
        assert_eq!(
            Uint::from_parts(vec![140]).to_radix(4, "😀😂😇😘"),
            Ok(String::from("😇😀😘😀"))
        );

        {
            let x = Uint::from_parts(vec![120]);
            let r = x.to_radix(5, "01233");
            assert_eq!(r, Ok(String::from("330")));
            assert_ne!(Uint::from_radix(&r.unwrap(), 5, "01233"), Ok(x));
        }

        assert_eq!(
            Uint::zero().to_radix(0, "A"),
            Err(ConversionError::ZeroBase)
        );
        assert_eq!(
            Uint::zero().to_radix(4, "A"),
            Err(ConversionError::AlphabetTooSmall)
        );
    }

    #[test]
    fn try_into_u64() {
        assert_eq!(Uint::zero().try_into(), Ok(0u64));
        assert_eq!(Uint::from_parts(vec![1849]).try_into(), Ok(1849u64));
        assert_eq!(Uint::from(9328421).try_into(), Ok(9328421u64));
        assert_eq!(Uint::from_parts(vec![23, 0, 0]).try_into(), Ok(23u64));
        assert_eq!(
            TryInto::<u64>::try_into(Uint::from_parts(vec![91, 1])),
            Err(())
        );
    }
}

/// Signed integer with arbitrary precision.
///
/// # Edge cases
///
/// Both `+0` and `-0` are valid values with a different binary representation, but their value are
/// considered equivalent.
///
/// # Implementation
///
/// The signed integer is implemented as a wrapper over `Uint` containing a sign bit.
/// Refer to the documentation of `Uint` for more details.
#[derive(Debug, Clone)]
pub struct Sint {
    pub(crate) abs: Uint,
    pub(crate) neg: bool,
}

impl Sint {
    /// Creates an `Sint` from the given data.
    ///
    /// # Arguments
    ///
    /// * `absolute` - `Uint` with the absolute value of the number
    /// * `negative` - `bool` determing whether the number is negative
    pub fn from_parts(absolute: Uint, negative: bool) -> Self {
        Self {
            abs: absolute,
            neg: negative,
        }
    }

    /// Returns the number of bits used to represent the absolute value of this number.
    /// This does not include the 1 bit used for storing the sign.
    pub fn bits(&self) -> usize {
        self.abs.bits()
    }

    /// Takes `self` to the power of `exponent`.
    ///
    /// # Panics
    ///
    /// Panics in the case of `0^0`.
    pub fn pow(self, exponent: u64) -> Self {
        Sint {
            abs: self.abs.pow(exponent),
            neg: self.neg && (exponent % 2 == 1),
        }
    }

    /// Creates new Sint from the given decimal string.
    ///
    /// # Arguments
    ///
    /// * `s` - Input string slice
    ///
    /// # Returns
    ///
    /// `ConversionError::UnknownCharacter` if the string contains a character other than `[0-9-]`.
    /// `Sint` representing the given number otherwise.
    pub fn from_decimal(s: &str) -> Result<Self, ConversionError> {
        Self::from_radix(s, 10, "0123456789", '-', '+')
    }

    /// Creates new Sint by parsing the input string in the given base.
    ///
    /// # Arguments
    ///
    /// * `s` - Input string slice
    /// * `base` - Base of input number. Must be `u64` due to the limitations of `Uint`.
    /// * `alphabet` - String slice containing characters in base. If the alphabet contains several
    /// of the same character, the left-most one will be used for conversion.
    /// * `minus` - The minus character. If the first character in the string is the `minus`, the
    /// number will be negative. It has no special meaning in any other position.
    /// * `plus` - The plus character. If the first character in the string is the `plus`, the
    /// number will be positive.
    ///
    /// # Returns
    ///
    /// `ConversionError::AlphabetTooSmall` if the alphabet doesn't have enough characters for the
    /// base.
    /// `ConversionError::UnknownCharacter` if the strings contains a character not in the alphabet.
    /// `ConversionError::EmptyString` if the input string is empty.
    /// `Sint` representing the given number if the input is correct.
    pub fn from_radix(
        s: &str,
        base: u64,
        alphabet: &str,
        minus: char,
        plus: char,
    ) -> Result<Self, ConversionError> {
        if s.is_empty() {
            Err(ConversionError::EmptyString)
        } else {
            match s.strip_prefix(minus) {
                Some(s) => Ok(Sint {
                    abs: Uint::from_radix(s, base, alphabet)?,
                    neg: true,
                }),
                None => match s.strip_prefix(plus) {
                    None => Ok(Sint {
                        abs: Uint::from_radix(s, base, alphabet)?,
                        neg: false,
                    }),
                    Some(s) => Ok(Sint {
                        abs: Uint::from_radix(s, base, alphabet)?,
                        neg: false,
                    }),
                },
            }
        }
    }

    /// Returns a String with the decimal representation of the number.
    pub fn to_decimal(&self) -> String {
        self.to_radix(10, "0123456789").unwrap()
    }

    /// Returns a String with the representation of the number in the given base and alphabet.
    /// If the number is negative, it is precedded, by the '-' character.
    ///
    /// See the documentation of Uint.to_radix() for details.
    pub fn to_radix(&self, base: u64, alphabet: &str) -> Result<String, ConversionError> {
        let mut s = self.abs.to_radix(base, alphabet)?;
        if self.lt(&0) {
            s.insert(0, '-');
        }
        Ok(s)
    }
}

impl Display for Sint {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{}", self.to_decimal())
    }
}

impl From<i64> for Sint {
    fn from(value: i64) -> Self {
        Self {
            abs: Uint::from(value.wrapping_abs() as u64),
            neg: value.is_negative(),
        }
    }
}

impl From<Uint> for Sint {
    fn from(value: Uint) -> Self {
        Self {
            abs: value,
            neg: false,
        }
    }
}

impl TryInto<i64> for &Sint {
    type Error = ();

    fn try_into(self) -> Result<i64, ()> {
        if self.abs.bits() <= 63 {
            let abs = TryInto::<u64>::try_into(&self.abs).unwrap() as i64;
            if self.neg {
                Ok(-abs)
            } else {
                Ok(abs)
            }
        } else {
            Err(())
        }
    }
}

impl TryInto<i64> for Sint {
    type Error = ();

    fn try_into(self) -> Result<i64, ()> {
        (&self).try_into()
    }
}

impl PartialEq for Sint {
    fn eq(&self, other: &Self) -> bool {
        self.abs == other.abs && (self.neg == other.neg || self.eq(&0))
    }
}

impl PartialEq<i64> for Sint {
    fn eq(&self, other: &i64) -> bool {
        self.try_into() == Ok(*other)
    }
}

impl Eq for Sint {}

impl PartialOrd for Sint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialOrd<i64> for Sint {
    fn partial_cmp(&self, other: &i64) -> Option<Ordering> {
        match TryInto::<i64>::try_into(self) {
            Ok(v) => v.partial_cmp(other),
            Err(()) => if self.neg {
                Some(Ordering::Less)
            } else {
                Some(Ordering::Greater)
            }
        }
    }
}

impl Ord for Sint {
    fn cmp(&self, other: &Self) -> Ordering {
        // 0 needs special handling since -0 and 0 need to be equal
        if self.eq(&0) && other.eq(&0) {
            return Ordering::Equal;
        }

        match (self.neg, other.neg) {
            (false, false) => self.abs.cmp(&other.abs),
            (true, true) => self.abs.cmp(&other.abs).reverse(),
            (false, true) => Ordering::Greater,
            (true, false) => Ordering::Less,
        }
    }
}

impl Add<i64> for Sint {
    type Output = Sint;
    fn add(self, other: i64) -> Sint {
        self + Sint::from(other)
    }
}

impl Add<&Sint> for Sint {
    type Output = Sint;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn add(self, other: &Sint) -> Sint {
        if self.neg == other.neg {
            Sint {
                abs: self.abs + &other.abs,
                neg: self.neg,
            }
        } else if self.abs >= other.abs {
            Sint {
                abs: self.abs - &other.abs,
                neg: self.neg,
            }
        } else {
            Sint {
                // TODO Get rid of this clone in the case of Add<Sint> for Sint (possibly by
                // using MaybeOwned)
                abs: other.abs.clone() - self.abs,
                neg: other.neg,
            }
        }
    }
}

impl Add<Sint> for Sint {
    type Output = Sint;
    fn add(self, other: Sint) -> Sint {
        self + &other
    }
}

impl Sub<i64> for Sint {
    type Output = Sint;
    fn sub(self, other: i64) -> Sint {
        self - Sint::from(other)
    }
}

impl Sub<&Sint> for Sint {
    type Output = Sint;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn sub(self, other: &Sint) -> Sint {
        if self.neg != other.neg {
            Sint {
                abs: self.abs + &other.abs,
                neg: self.neg,
            }
        } else if self.abs >= other.abs {
            Sint {
                abs: self.abs - &other.abs,
                neg: self.neg,
            }
        } else {
            Sint {
                // TODO Get rid of this clone in the case of Add<Sint> for Sint (possibly by
                // using MaybeOwned)
                abs: other.abs.clone() - self.abs,
                neg: !other.neg,
            }
        }
    }
}

impl Sub<Sint> for Sint {
    type Output = Sint;
    fn sub(self, other: Sint) -> Sint {
        self - &other
    }
}

impl Mul<i64> for Sint {
    type Output = Sint;
    fn mul(self, other: i64) -> Sint {
        self * Sint::from(other)
    }
}

impl Mul<&Sint> for Sint {
    type Output = Sint;
    fn mul(self, other: &Sint) -> Sint {
        self * other.clone()
    }
}

impl Mul<Sint> for Sint {
    type Output = Sint;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn mul(self, other: Sint) -> Sint {
        Sint {
            abs: self.abs * other.abs,
            neg: self.neg ^ other.neg,
        }
    }
}

impl Mul<&Uint> for Sint {
    type Output = Sint;
    fn mul(self, other: &Uint) -> Sint {
        self * other.clone()
    }
}

impl Mul<Uint> for Sint {
    type Output = Sint;

    fn mul(self, other: Uint) -> Sint {
        Sint {
            abs: self.abs * other,
            neg: self.neg,
        }
    }
}

impl Div<i64> for Sint {
    type Output = Sint;
    fn div(self, other: i64) -> Sint {
        // TODO Optimize this using Div<u64> for Uint
        self / Sint::from(other)
    }
}

impl Div<&Sint> for Sint {
    type Output = Sint;
    fn div(self, other: &Sint) -> Sint {
        self / other.clone()
    }
}

impl Div<Sint> for Sint {
    type Output = Sint;

    #[allow(clippy::suspicious_arithmetic_impl)]
    fn div(self, other: Sint) -> Sint {
        Sint {
            abs: self.abs / other.abs,
            neg: self.neg ^ other.neg,
        }
    }
}

impl Neg for Sint {
    type Output = Sint;

    fn neg(self) -> Sint {
        Sint {
            abs: self.abs,
            neg: !self.neg,
        }
    }
}

impl Shl<usize> for Sint {
    type Output = Sint;

    fn shl(mut self, shift: usize) -> Sint {
        self.abs = self.abs << shift;
        self
    }
}

impl Shr<usize> for Sint {
    type Output = Sint;

    /// Note: This shift performs truncation towards zero, unlike a standard arithmetic shift
    /// which rounds towards `-inf`.
    fn shr(mut self, shift: usize) -> Sint {
        self.abs = self.abs >> shift;
        self
    }
}

#[cfg(test)]
mod sint_tests {
    use crate::bigint::{Sint, Uint};
    use std::cmp::Ordering;
    use std::panic::catch_unwind;

    #[test]
    fn eq() {
        let zerop = Sint::from_parts(Uint::from(0), false);
        let zeron = Sint::from_parts(Uint::from(0), true);
        let pos = Sint::from_parts(Uint::from(1), false);
        let neg = Sint::from_parts(Uint::from(1), true);

        assert_eq!(zerop, zeron);
        assert_ne!(zerop, pos);
        assert_ne!(zerop, neg);
        assert_ne!(zeron, pos);
        assert_ne!(zeron, neg);

        assert_ne!(pos, neg);
    }

    #[test]
    fn eq_i64() {
        let zerop = Sint::from_parts(Uint::from(0), false);
        let zeron = Sint::from_parts(Uint::from(0), true);
        let pos = Sint::from_parts(Uint::from(1), false);
        let neg = Sint::from_parts(Uint::from(1), true);

        assert_eq!(zerop, 0);
        assert_eq!(zeron, 0);

        assert_ne!(pos, 0);
        assert_eq!(pos, 1);
        assert_ne!(pos, -1);

        assert_ne!(neg, 0);
        assert_ne!(neg, 1);
        assert_eq!(neg, -1);
    }

    #[test]
    fn cmp() {
        let zerop = Sint::from_parts(Uint::from(0), false);
        let zeron = Sint::from_parts(Uint::from(0), true);
        let pos = Sint::from_parts(Uint::from(1), false);
        let neg = Sint::from_parts(Uint::from(1), true);
        let verypos = Sint::from_parts(Uint::from_parts(vec![1, 2]), false);
        let veryneg = Sint::from_parts(Uint::from_parts(vec![1, 2]), true);

        assert_eq!(zerop.cmp(&zeron), Ordering::Equal);
        assert_eq!(zeron.cmp(&zerop), Ordering::Equal);

        assert_eq!(zerop.cmp(&pos), Ordering::Less);
        assert_eq!(pos.cmp(&zerop), Ordering::Greater);
        assert_eq!(pos.cmp(&verypos), Ordering::Less);
        assert_eq!(verypos.cmp(&pos), Ordering::Greater);

        assert_eq!(zeron.cmp(&neg), Ordering::Greater);
        assert_eq!(neg.cmp(&zeron), Ordering::Less);
        assert_eq!(neg.cmp(&veryneg), Ordering::Greater);
        assert_eq!(veryneg.cmp(&neg), Ordering::Less);

        assert_eq!(pos.cmp(&neg), Ordering::Greater);
        assert_eq!(veryneg.cmp(&verypos), Ordering::Less);
    }

    #[test]
    fn cmp_i64() {
        let zerop = Sint::from_parts(Uint::from(0), false);
        let zeron = Sint::from_parts(Uint::from(0), true);
        let pos = Sint::from(3);
        let neg = Sint::from(-3);
        let large_pos = Sint::from(Uint::from_parts(vec![0, 1]));
        let large_neg = -large_pos.clone();

        assert_eq!(zerop.partial_cmp(&0), Some(Ordering::Equal));
        assert_eq!(zerop.partial_cmp(&1), Some(Ordering::Less));
        assert_eq!(zerop.partial_cmp(&-1), Some(Ordering::Greater));

        assert_eq!(zeron.partial_cmp(&0), Some(Ordering::Equal));
        assert_eq!(zeron.partial_cmp(&1), Some(Ordering::Less));
        assert_eq!(zeron.partial_cmp(&-1), Some(Ordering::Greater));

        assert_eq!(pos.partial_cmp(&0), Some(Ordering::Greater));
        assert_eq!(pos.partial_cmp(&3), Some(Ordering::Equal));
        assert_eq!(pos.partial_cmp(&-3), Some(Ordering::Greater));
        assert_eq!(pos.partial_cmp(&i64::MAX), Some(Ordering::Less));
        assert_eq!(pos.partial_cmp(&i64::MIN), Some(Ordering::Greater));

        assert_eq!(neg.partial_cmp(&0), Some(Ordering::Less));
        assert_eq!(neg.partial_cmp(&3), Some(Ordering::Less));
        assert_eq!(neg.partial_cmp(&-3), Some(Ordering::Equal));
        assert_eq!(neg.partial_cmp(&i64::MAX), Some(Ordering::Less));
        assert_eq!(neg.partial_cmp(&i64::MIN), Some(Ordering::Greater));

        assert_eq!(large_pos.partial_cmp(&0), Some(Ordering::Greater));
        assert_eq!(large_pos.partial_cmp(&i64::MAX), Some(Ordering::Greater));
        assert_eq!(large_pos.partial_cmp(&i64::MIN), Some(Ordering::Greater));

        assert_eq!(large_neg.partial_cmp(&0), Some(Ordering::Less));
        assert_eq!(large_neg.partial_cmp(&i64::MAX), Some(Ordering::Less));
        assert_eq!(large_neg.partial_cmp(&i64::MIN), Some(Ordering::Less));
    }

    #[test]
    fn neg() {
        assert_eq!(-Sint::from(3), Sint::from(-3));
        assert_eq!(-Sint::from(667), Sint::from(-667));
        assert_eq!(-Sint::from(-12), Sint::from(12));
        assert_eq!(-Sint::from(0), Sint::from(0));
    }

    #[test]
    fn pow() {
        assert_eq!(Sint::from(0).pow(1), Sint::from(0));
        assert_eq!(Sint::from(-1).pow(0), Sint::from(1));
        assert_eq!(Sint::from(-3).pow(3), Sint::from(-27));
        assert_eq!(Sint::from(-5).pow(4), Sint::from(625));

        assert!(catch_unwind(|| Sint::from(0).pow(0)).is_err());
    }

    #[test]
    fn from_i64() {
        assert_eq!(Sint::from_parts(Uint::from(0), true), Sint::from(0));
        assert_eq!(Sint::from_parts(Uint::from(7843), false), Sint::from(7843));
        assert_eq!(Sint::from_parts(Uint::from(192), true), Sint::from(-192));

        assert_eq!(
            Sint::from_parts(Uint::from((1 << 63) - 1), false),
            Sint::from(i64::MAX)
        );
        assert_eq!(
            Sint::from_parts(Uint::from(1 << 63), true),
            Sint::from(i64::MIN)
        );
    }

    #[test]
    fn arithmetic_ops() {
        // This only tests the behaviour with different signs.
        // The actual operations are delegated to `Uint` and tested there.
        let test_op = |a, b, c, op: fn(Sint, Sint) -> Sint| {
            assert_eq!(op(Sint::from(a), Sint::from(b)), Sint::from(c))
        };
        let test_add = |a, b, c| test_op(a, b, c, std::ops::Add::add);
        test_add(16, 23, 39);
        test_add(-16, -23, -39);
        test_add(7, -5, 2);
        test_add(5, -7, -2);
        test_add(-10, 13, 3);
        test_add(-13, 10, -3);

        let test_sub = |a, b, c| test_op(a, b, c, std::ops::Sub::sub);
        test_sub(-7, 37, -44);
        test_sub(7, -37, 44);
        test_sub(15, 9, 6);
        test_sub(9, 15, -6);
        test_sub(-36, -12, -24);
        test_sub(-12, -36, 24);

        let test_mul = |a, b, c| test_op(a, b, c, std::ops::Mul::mul);
        test_mul(5, 10, 50);
        test_mul(-7, 6, -42);
        test_mul(1, -8, -8);
        test_mul(-91, -4, 364);

        let test_div = |a, b, c| test_op(a, b, c, std::ops::Div::div);
        test_div(16, 4, 4);
        test_div(15, -6, -2);
        test_div(-77, 11, -7);
        test_div(-43, -4, 10);

        let test_shr = |a, b, c| assert_eq!(Sint::from(a) >> b, Sint::from(c));
        test_shr(143, 4, 8);
        test_shr(-143, 4, -8); // NOT -9
        test_shr(8, 10, 0);
        test_shr(-1, 0, -1);
        test_shr(-7192, 4, -449); // NOT -450

        let test_shl = |a, b, c| assert_eq!(Sint::from(a) << b, Sint::from(c));
        test_shl(0, 100, 0);
        test_shl(5, 4, 80);
        test_shl(-50, 0, -50);
        test_shl(-15, 10, -15360);
    }
}

#[cfg(test)]
/// Benchmarks for the runtime of basic operations.
/// Meant for detecting regressions and comparing implementations.
///
/// Note: These benchmarks only test `Uint` since `Sint` is simply a thin wrapper around it. If
/// this changes (it shouldn't) more benchmarks need to be added.
mod bench {
    use super::*;
    extern crate test;
    use test::bench::{black_box, Bencher};

    #[bench]
    fn add(b: &mut Bencher) {
        let values_x: Vec<u64> = ((u64::MAX / 2 - 100)..(u64::MAX / 2 + 100)).collect();
        let values_y: Vec<u64> = values_x
            .clone()
            .into_iter()
            .chain(std::iter::once(u64::MAX))
            .collect();
        b.iter(|| {
            black_box(Uint::from_parts(values_y.clone()) + Uint::from_parts(values_x.clone()))
        })
    }

    #[bench]
    fn sub(b: &mut Bencher) {
        let values_x: Vec<u64> = ((u64::MAX - 200)..u64::MAX).collect();
        let values_y: Vec<u64> = values_x
            .clone()
            .into_iter()
            .chain(std::iter::once(u64::MAX))
            .collect();
        b.iter(|| {
            black_box(Uint::from_parts(values_y.clone()) - Uint::from_parts(values_x.clone()))
        })
    }

    #[bench]
    fn mul(b: &mut Bencher) {
        let values_x: Vec<u64> = ((u64::MAX / 2 - 10)..(u64::MAX / 2 + 10)).collect();
        let values_y: Vec<u64> = values_x
            .clone()
            .into_iter()
            .chain(std::iter::once(u64::MAX))
            .collect();
        b.iter(|| {
            black_box(Uint::from_parts(values_y.clone()) * Uint::from_parts(values_x.clone()))
        })
    }

    #[bench]
    fn mul_u64(b: &mut Bencher) {
        let values: Vec<u64> = ((u64::MAX / 2 - 100)..(u64::MAX / 2 + 100)).collect();
        b.iter(|| black_box(Uint::from_parts(values.clone()) * (u64::MAX / 2 + 1)))
    }

    #[bench]
    fn div(b: &mut Bencher) {
        let values_x: Vec<u64> = ((u64::MAX / 2 - 5)..(u64::MAX / 2 + 5)).collect();
        let values_y: Vec<u64> = ((u64::MAX / 2 - 10)..(u64::MAX / 2 + 10)).collect();
        b.iter(|| {
            black_box(Uint::from_parts(values_y.clone()) / Uint::from_parts(values_x.clone()))
        })
    }

    #[bench]
    fn div_u64(b: &mut Bencher) {
        let values: Vec<u64> = ((u64::MAX / 2 - 100)..(u64::MAX / 2 + 100)).collect();
        b.iter(|| black_box(Uint::from_parts(values.clone()) / (u64::MAX / 2 - 1)))
    }

    #[bench]
    fn rem(b: &mut Bencher) {
        let values_x: Vec<u64> = ((u64::MAX / 2 - 5)..(u64::MAX / 2 + 5)).collect();
        let values_y: Vec<u64> = ((u64::MAX / 2 - 10)..(u64::MAX / 2 + 10)).collect();
        b.iter(|| {
            black_box(Uint::from_parts(values_y.clone()) % Uint::from_parts(values_x.clone()))
        })
    }

    #[bench]
    fn rem_u64(b: &mut Bencher) {
        let values: Vec<u64> = ((u64::MAX / 2 - 100)..(u64::MAX / 2 + 100)).collect();
        b.iter(|| black_box(Uint::from_parts(values.clone()) % (u64::MAX / 2 - 1)))
    }

    #[bench]
    fn pow(b: &mut Bencher) {
        b.iter(|| Uint::from(7).pow(1000))
    }

    #[bench]
    fn pow_mod(b: &mut Bencher) {
        b.iter(|| Uint::from(101).pow_mod(&Uint::from(65537), &Uint::from_parts(vec![1, 1])))
    }
}
